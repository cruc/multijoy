a) Hardware requirements
------------------------

This is a Multijoy game. You can play it with up to 6 players simultaneously.
The minimum hardware requirements are:

        * 80386 CPU (or better), since the code contains 32 bit instructions
        * SVGA video card capable of displaying 640x480 pixels in 256 colors
          (all modern video cards can do that, of course)
        * VESA driver if your video chipset is not recognized by the program
          (see below for a list of supported chipsets)
        * a Multi Joystick Interface (see d) if you don't have one)

b) Supported chipsets
---------------------

Load a VESA driver if you DON'T have one of the following video chipsets!
Currently detected and supported chipsets are:

        Cirrus  54xx
        Oak     OTI-067 (untested)
        Oak     OTI-077 (untested)
        S3      86C911  (untested)
        Trident TR8900
        Trident TR9000
        Tseng   ET3000  (untested)
        Tseng   ET4000
        WD      WD90Cxx

Using a VESA driver slows things down a bit, but works fine for VESA BIOS
versions 1.2 (and above?). If you encounter difficulties with one of
the direct supported chipsets, you can also force the program to use
the VESA driver overriding the autodetection by setting a DOS environment
variable. At the DOS prompt, just enter

        SET VESA=X


c) Where to find a VESA driver
------------------------------

Well, there comes a good one with the SVGA kit by Kendall Bennet called
UNIVESA. It supports a lot more chipsets than my program, so give it a try.
You should find it (according to Kendall's docs) on SIMTEL-20 and garbo, or
the latest version at

        godzilla.cgl.rmit.oz.au : kjb/MGL/svgakt??.zip

d) How to play without an interface
-----------------------------------

This game can be played by two players using the keyboard. Just set the
DOS environment variables

        SET MULTIPATH=<path to this game>
        SET MULTICFG=keyboard

But remember, it's MUCH more fun with six players!

You can download the information you need to build one from the ftp
server given in the next paragraph.


e) Where to find the latest version of this game
------------------------------------------------

You will always find the latest version of this game and the Multijoy SDK
on the 'official MULTIJOY ftp server'

        ftp.tu-clausthal.de     : /pub/msdos/games/multijoy

For those of you without ftp access, there is also a mail server. Just mail
a message with 'help' as body to

        mail-server@ftp.tu-clausthal.de

to see how it works.


f) How to contact the author
----------------------------

Any bug reports, suggestions, questions and comments are welcome.
Well, bug reports are not welcome, but most important. So don't hesitate
to contact the author!

Either by Email at

        incr@asterix.rz.tu-clausthal.de
        (that's not my account, so the answer may take some time)

or by snail mail at

        Peter Fiege
        Tilsiter Strasse 6
        25421 Pinneberg
        Germany


g) Legal stuff
--------------

Multris - Copyright (C) 1993-94 Peter Fiege

All Rights Reserved.

Multris is not public domain software. It is copyrighted software, Copyright
(C) 1993,94 Peter Fiege. It is however free software, or what some people term
'Freeware'. You may NOT however re-distribute modified versions of the game.
You MAY copy it, give it to your friends, upload it to a BBS or something
similar, but: Don't charge any money for it and only distribute the whole
original archive, with all the files included. If you want to include this
game on a CD ROM, you must send me a free copy of the CD.

Since this software is free, it is supplied WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE. It is supplied as it, in the hope the people will find it fun, and
as an example for a multi joystick game. I AM NOT RESPONSIBLE FOR ANY DAMAGE
RESULTING FROM USE OR ABUSE OF THE SOFTWARE.
