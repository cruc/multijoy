unit graf13;

interface

uses dos,globvar;

const bcol = 100;

var br,sbr:integer;

procedure rectangle(x,y,b,h:integer; col:byte);
procedure frectangle(x,y,b,h:integer; col:byte);
procedure plot(x,y:integer;color:byte);
procedure ploti(x,y:integer;color:byte);
procedure plot_b(x,y:integer);
function  getcol(x,y:integer):byte;
procedure set_palette;
procedure prepare_palette;
procedure init_video;
procedure reset_video;
procedure clear(c:byte);

implementation

procedure rectangle(x,y,b,h:integer; col:byte);
label l1,l2;
begin
  if (h=0) or (b=0) then exit;
  asm

    DEC b
    DEC h

    MOV AX,$A000
    MOV ES,AX

    MOV BX,320
    MOV AX,y
    MUL BX

    ADD AX,x
    MOV DI,AX

    MOV AL,col

    MOV si,di

    MOV CX,h

    MOV BX,b

l1: MOV [ES:DI],AL
    MOV [ES:DI+BX],AL
    ADD DI,320
    LOOP l1

    MOV CX,b

l2: MOV [ES:DI],AL
    MOV [ES:SI],AL
    INC DI
    INC SI
    LOOP l2

    MOV [ES:DI],AL

  end;
end;

procedure frectangle(x,y,b,h:integer; col:byte);
label l1,l2;
begin
  if (h=0) or (b=0) then exit;
  asm


    MOV AX,$A000
    MOV ES,AX

    MOV BX,320
    MOV AX,y
    MUL BX

    ADD AX,x
    MOV DI,AX

    MOV AL,col

    MOV DX,h

l1: PUSH DI

    MOV CX,b
    REP STOSB

    POP DI
    ADD DI,320

    DEC DX
    JNZ l1
  end;
end;

procedure plot(x,y:integer;color:byte);
label fehler;
begin
  asm
    CMP x,0
    JL  fehler
    CMP x,319
    JG  fehler
    CMP y,0
    JL  fehler
    CMP y,199
    JG  fehler

    MOV AX,$A000
    MOV ES,AX

    MOV BX,320
    MOV AX,y
    MUL BX

    ADD AX,x
    MOV DI,AX

    MOV AL,color

    MOV [ES:DI],AL

fehler:
  end;
end;

procedure ploti(x,y:integer;color:byte);
label fehler;
begin
  asm
    CMP x,0
    JL  fehler
    CMP x,319
    JG  fehler
    CMP y,0
    JL  fehler
    CMP y,199
    JG  fehler

    MOV AX,$A000
    MOV ES,AX

    MOV BX,320
    MOV AX,y
    MUL BX

    ADD AX,x
    MOV DI,AX

    MOV AL,color

    XOR [ES:DI],AL

fehler:
  end;
(*  if (x>=0) and (x<=319) and (y>=0) and (y<=199) then
    mem[$A000:word(y*320+x)]:=color xor mem[$A000:word(y*320+x)];*)
end;

procedure plot_b(x,y:integer);
begin
  ploti(x,y,63);
  ploti(x-br,y,bcol);
  ploti(x+br,y,bcol);
  ploti(x,y-br,bcol);
  ploti(x,y+br,bcol);
  ploti(x-sbr,y-sbr,bcol);
  ploti(x+sbr,y+sbr,bcol);
  ploti(x+sbr,y-sbr,bcol);
  ploti(x-sbr,y+sbr,bcol);
end;

function getcol(x,y:integer):byte;
begin
  getcol := mem[$A000:word(y*320+x)];
end;

procedure set_palette;
var
  i,j : byte;
begin
  port[$3C8] := 0;
  inline($FA);
  for i:=0 to 63 do
    for j:=1 to 3 do
      port[$3C9] := i;
  port[$3c9] := 0;
  port[$3c9] := 0;
  port[$3c9] := 0;
  if not color_ then
    for i:=1 to 6 do
      for j:=1 to 3 do
        port[$3c9] := spcol[i]
  else
    for i:=1 to 6 do
      for j:=1 to 3 do
        port[$3c9] := spcolc[i,j];
  port[$3c8] := bcol;
  port[$3c9] := 43;
  port[$3c9] := 43;
  port[$3c9] := 43;
  inline($FB);
end;

procedure prepare_palette;
var i:integer;
    l:longint;
begin
  l:=randseed;
  randseed := 101;
  inline($fa);
  port[$3c8]:=0;
  port[$3c9]:= 0;port[$3c9]:= 0;port[$3c9]:= 0;
  port[$3c9]:=63;port[$3c9]:= 31;port[$3c9]:= 31;
  port[$3c9]:= 31;port[$3c9]:=63;port[$3c9]:= 31;
  port[$3c9]:= 31;port[$3c9]:= 31;port[$3c9]:=63;
  port[$3c9]:=63;port[$3c9]:=63;port[$3c9]:= 31;
  port[$3c9]:= 31;port[$3c9]:=63;port[$3c9]:=63;
  port[$3c9]:=63;port[$3c9]:= 31;port[$3c9]:=63;
  port[$3c9]:=63;port[$3c9]:=63;port[$3c9]:=63;
  port[$3c9]:=0;port[$3c9]:= 63;port[$3c9]:=0;
  port[$3c9]:=63;port[$3c9]:=0;port[$3c9]:=0;
  port[$3c8]:=51;
  port[$3c9]:=33;port[$3c9]:= 21;port[$3c9]:= 21;
  port[$3c9]:= 21;port[$3c9]:=33;port[$3c9]:= 21;
  port[$3c9]:= 21;port[$3c9]:= 21;port[$3c9]:=33;
  port[$3c9]:=33;port[$3c9]:=33;port[$3c9]:= 21;
  port[$3c9]:= 21;port[$3c9]:=33;port[$3c9]:=33;
  port[$3c9]:=33;port[$3c9]:= 21;port[$3c9]:=33;
  port[$3c8]:=128;
  for i:=1 to 128*3 do port[$3c9]:=random(64);
  inline($fb);
  randseed := l;
end;

procedure init_video;
var reg:registers;
begin
  reg.ax := $0F00;
  intr($10,reg);
  savemode := reg.al;

  reg.ax := $13;
  intr($10,reg);

  set_palette;
end;

procedure reset_video;
var reg:registers;
begin
  reg.ax := savemode;
  intr($10,reg);
end;

procedure clear(c:byte); assembler;
asm
  CLD
  MOV AX,$a000
  MOV ES,AX

  MOV DI,0
  MOV AL,c
  MOV AH,AL
  MOV CX,320*100

  REP STOSW
end;




end.