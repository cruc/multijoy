name noise
carrier    adsr_eg   15 15 0 15 1 level 10
modulator  adsr_eg   15 15 0 15 1 level 10
feedbackfm 1 0
next

name guided
carrier    adsr_eg   15 15 0 7 1
modulator  adsr_eg   15 15 0 7 1
feedbackfm 6 0
next

name booma
amvib 1 0
carrier    adsr_eg   15 15 0 5 1
modulator  adsr_eg   15 15 0 5 1
feedbackfm 5 0
next

name homing
carrier    adsr_eg   15 15 0 6 1
modulator  adsr_eg   15 15 0 6 1
feedbackfm 6 0
next

name boomb
carrier    adsr_eg   15 15 0 5 1
modulator  adsr_eg   15 15 0 5 1
feedbackfm 0 0
next

save_ibk asound
