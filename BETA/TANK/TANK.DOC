01234567abcd89
/0
=Help on TANK BATTLE

This help is intended to tell you how to play
TANK BATTLE, and also gives some advice on how to
become a successful TANK BATTLE veteran.

-0) Index

1) Score screen
2) Controls
3) Game objectives
4) Weaponry
5) Maneuvring
6) Scoring
7) Tactics
8) TANK BATTLE origins
9) More about TANK BATTLE

/1
=1) Score screen

The following keys are available:

ESC                  -  exits to DOS
SPACE                -  starts the game
FN                   -  toggles player N
ALT-FN               -  changes name of player N
R                    -  resets scores
T                    -  alters team compositions
O                    -  moves to option screen
H                    -  this help

In the option screen you can adapt various
settings of TANK BATTLE to your preferences.

/2
=2) Controls

stick left                        -  left  turn
stick right                       -  right turn
stick forward                     -  go ahead
stick back                        -  reverse

short button press                -  fire cannon
long  button press                -  lay a mine

or, if you have a 2- or 3-button joystick:

fire  button                      -  fire cannon
extra button                      -  lay a mine

TANK BATTLE starts with ordinary joystick control.
Each player can switch to 2-button joystick
control by pressing the extra button during game
play. (This will, of course, immediately lay a
mine.)
/3
=3) Game objectives

Pretty simple: Destroy all enemy tanks and score
as many points as you can.

A single match ends, when only tanks of one team
survive or the players, perhaps because of ammo
shortage, agree that the game is a draw and press
SPACE to end the game.
/4
=4) Weaponry

The means of fighting enemy tanks are shells and
mines. Your tank usually is equipped with 30
shells for the cannon and 5 anti-tank mines. A
tank needs 5 hits, wether by shells or by mines,
to be destroyed.

Cannon shells travel at high speed and hit with
great precision. Only massive objects such as
houses or hangars can stop cannon shells. However,
if a shell crosses a grove of trees, there is a
chance that it hits a tree trunk or a strong
branch. This causes the impact fuse to blow up the
shell immediately, so the shot can be considered a
miss even if it was well-aimed.
/
Mines are very hard to see for a tank driver -
actually, they are invisible. You know that you
have placed a mine when you release the button and
your tank does not fire. The other players never
know that you have placed a mine. Mines are armed
the instant the mine laying tank moves on. If the
tank does not move away from the spot where it
placed the mine, nothing will happen to it as the
mine is not armed yet.
/5
=5) Maneuvring

The tanks go as fast backward as they go forward,
but the terrain has great influence on the tanks'
speed:

on streets and asphalt surface        top speed!
in tunnels and hangars                3/4 speed
on grass                              2/3 speed
on sand or cratered ground            1/2 speed
under trees                           1/3 speed
in water                               no speed

The speed drops due to increased friction on soft
ground such as grass or sand or due to the
difficulties of maneuvring around obstacles or in
limited spaces. Driving under trees slows down a
tank as he has to roll down the undergrowth as
well as to avoid the tree trunks. Take care that
you don't move your tank too far into water as you
perhaps will get bogged down.
/6
=6) Scoring

Surviving one other team          -   +1 point

ESC aborts the game without counting the score.
If the remaining players in the game agree that
this game is a draw, just press SPACE to keep the
score won so far.
/7a
=7) Tactics

a) The key to success
b) Mistakes to avoid
c) Successful tactics
d) Tankers' tricks

-a) The key to success

To win a game of TANK BATTLE, you must avoid
losing. Pretty obvious, you may think, but I put
it this way to emphasize the defensive element of
tactics. If you are cautious, you will be hard to
defeat even by a skilled opponent. If you make a
mistake, it needs only a novice tank driver to
exploit your weakness.

Another reason for stressing caution is that all
agree about what is a mistake, but opinions about
the most successful tactics diverge.
/b
-b) Mistakes to avoid

Let's have a look at the obvious mistakes one
could make.

#1 Getting hit and not running away.

A tank driver who has his gun trained on the enemy
usually does not like to flee from this enemy but
tries to defeat him by slugging it out. However,
the attacker shot first and scores always at least
one hit more than the defender. So take it as a
general rule: If you are hit, run away. It's a
good idea to run away in reverse gear so that you
can shoot at the attacker, and if you don't find
any cover, you may decide to zig-zag, firing each
time your gun is pointing at the attacker.
/
#2 Driving at right angle to the attacker.

If the attacker is within range, trying to run for
cover to the right or to the left is stupid. You
fail to threaten the attacker with your gun,
allowing him freedom of movement. You expose your
flank to his fire, making a good target for his
shells. If you run into an obstacle trying to
reach cover, things will even get worse for you.
Retreat only backwards or when shielded by cover.
If the enemy advances at you and you can't
retreat, fire at him. If you are desperate, charge
him head-on, as this threat may cause him to
retreat behind cover and loose ground you can use
for maneuvring.

/
#3 Attacking without waiting for your comrades.

That's the mistake that killed General Custer.
Attacking single enemy tanks is not as suicidal as
attacking groups of them, but if your comrades are
far away or reacting slowly, the enemy could move
in a second tank to outflank you.


#4 Not supporting your comrades' attack.

Whatever tactical advantage you expect to gain
from waiting or driving elsewhere, it will not
outweigh the destruction of your friends' tanks if
they are overwhelmed by superior forces.
/c
-c) Successful tactics

These are the basics of successful tactics:

#1 Use cover and avoid slow ground. Cover shields
you from attack from unsuspected directions, while
slow ground makes it hard to dodge enemy shots and
to run away or to attack quickly.

#2 Choose a position from that you can retreat and
move before you get encircled. If you can't
retreat, you will get outflanked and crushed
between two enemies, and if you can retreat but
don't do it, the same will happen.

#3 Keep close to your friends, and advance
parallel to and in co-operation with them. This
way, you can outflank the enemy, concentrate your
fire on one opponent and avoid being outnumbered.
/
#4 If you are outnumbered, attack quickly and
hard. Being shot up badly but facing one enemy
tank usually is better than being undamaged and
facing two enemies.

#5 Use mines only when the fight nears the end.
While minelaying is ineffective when all tanks are
undamaged and the battle is moving quickly, tanks
that have been hit during the initial clash will
fall victim to a mine easily. Mines give you the
opportunity to damage the enemy without risking
getting hit.
/d
-d) Tankers' tricks

And that's how some veteran TANK BATTLE players
fight:

CHRISTOF moves quickly, fires out of the move, and
is expert at advancing, firing around a corner and
retreating without getting hit. He also likes to
charge an enemy zig-zagging and firing each time
his gun points at his victim, hitting with almost
every shot.

MARCUS prefers sniper tactics. He aims carefully,
often without wasting a shell as 'tracer'. He
anticipates his opponents moves and hits with
remarkable precision from great distances.

HENNING likes to drive from cover to cover,
avoiding long-distance duels. Firing while
retreating and charging surprisingly to point-
blank range are part of his tactics, and he always
lays lots of mines.
/8
=8) TANK BATTLE origins

Atari VCS 2600 - COMBAT
  2 player game. Featured tank game, jet game and
  biplane game. The tanks battled in labyrinths,
  and the tanks' shells ricocheted off the walls
  or could be guided in flight.

Matel Intellivision - ARMOR BATTLE
  Two player game with a 45 degree perspective.
  Four tanks fighting on 274 different sceneries,
  consisting of asphalt, gras, water, trees and
  buildings.

Commodore Amiga - TANK
  By the same author as the Multijoy TANK BATTLE.
  Two player game similar to ARMOR BATTLE, but
  already used bird's-eye view. Sceneries were
  designed with an integrated scenery editor.
/9
=9) More about TANK BATTLE

Of course, you may have questions this text didn't
answer. If need to know more about the details or
if you just are curious about some aspect of the
game, don't hesitate to contact the author!

Either by Email at

   katte@tlaloc.in.tu-clausthal.de

or by snail mail at

   Henning Katte
   Siebensternweg 16
   38678 Clausthal-Zellerfeld
   Germany
