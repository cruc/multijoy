code	segment byte public
	assume cs:code, ds:code
	public readtimer,inittimer,restoretimer

counter	equ	0
corr	equ	2
tport	equ	40h                    ; Counter 0 Time of Day Clock
tctrl	equ	43h                    ; Mode Control Register
ictrl	equ	20h                    ; Programmable Interrupt Controller


inittimer	proc far

	mov	al,34h                 ; counter 0, LSB+MSB, mode 2, binary
	jmp	skip1

restoretimer	proc far

	mov	al,36h                 ; counter 0, LSB+MSB, mode 3, binary
skip1:	out	tctrl,al               ; prepare PIT
	mov	cx,2
itl:	mov	al,0                   ; set timer count to 0
	out	tport,al               ;
	loop	itl                    ; why loop?
	ret

restoretimer	endp
inittimer	endp



readtimer	proc far

      push bp
      mov  bp,sp
      push ds            ;end init

      ;SAVE IRQ-COUNTER TO BX
mark: mov  ax,40h
      mov  ds,ax
      mov  bx,word ptr ds:[6ch]

      mov  al,4h
      out  tctrl,al
      in   al,tport      ;save PIT LO-byte
      mov  cl,al
      in   al,tport      ;save PIT HI-byte
      mov  ch,al
                         ;PIT now in CX

      ;SAVE IRQ-COUNTER TO DX
      mov  ax,40h
      mov  ds,ax
      mov  dx,word ptr ds:[6ch]


      cmp  bx,dx         ;not equal then reread timer
      jne  mark          ;jump if not equal

      and  dh,01111111b  ;clear HI-bit to fit in maxlongint
      mov  ax,cx         ;PIT NOW IN AX
      not  ax            ;invert timer value (decreasing to increasing)

exit: pop  ds
      mov  [bp-4],ax     ;save result
      mov  [bp-2],dx
      mov  sp,bp
      pop  bp
      ret

readtimer	endp

; function result is always in ax(lo)+dx(hi)

code	ends
        end
