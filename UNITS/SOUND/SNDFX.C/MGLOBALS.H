/*      MGLOBALS.H
 *
 * MIDAS Sound System global variables
 *
 * Copyright 1994 Petteri Kangaslampi and Jarno Paananen
 *
 * This file is part of the MIDAS Sound System, and may only be
 * used, modified and distributed under the terms of the MIDAS
 * Sound System license, LICENSE.TXT. By continuing to use,
 * modify or distribute this file you indicate that you have
 * read the license and understand and accept it fully.
*/

#ifndef __MGLOBALS_H
#define __MGLOBALS_H


extern short    useEMS;                 /* should EMS be used? */
extern short    forceEMS;               /* should _only_ EMS be used? */
extern short    loadError;              /* module loading error */
extern short    ALE;                    /* should Amiga loops be emulated */
extern short    ptTempo;                /* should PT modules use tempo */
extern short    usePanning;             /* should PT modules use cmd 8 for
                                           panning? */
extern short    surround;               /* should Surround be enabled?
                                           (mainly for GUS)*/
extern short    realVU;                 /* use real VU meters? */

#endif
