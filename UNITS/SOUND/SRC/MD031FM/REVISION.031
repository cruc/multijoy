        MIDAS Sound System v0.31 Revision History
	-----------------------------------------

05-Jul-1994 (PK)
----------------
*.*
        MIDAS Sound System v0.31a. The first official release.




        Changes to revision b
        ---------------------

08-Jul-1994 (PK)
----------------
SB.ASM
        Fixed a bug with some SB DSP 1.xx not playing at all, as
        the speaker was being turned on again after starting the
        sound.

MOD.ASM
S3M.ASM
        Fixed "Constant too large" errors with TASM < 4.0, by
        changing a couple of "panLeft" arguments to "panLeft and
        0FFFFh". It seems that TASM versions before 4.0 consider
        a constant with value -64 (such as panLeft) to be
        32-bit.

MIDAS.C
MIDAS.PAS
        Added a new argument to midasPlayModule, which gives the
        number of channels to open for sound effects. This
        effectively breaks all existing code using the simple
        MIDAS interface, but the change needed is trivial: Just
        replace midasPlayModule(fileName) with
        midasPlayModule(fileName, 0).
        Note that midasStopModule() closes also the channels
        opened for sound effects. It is probably useful in
        games to open the required amount of channels in the
        beginning of the program and not to close them until
        exit even when playing different modules.



08-Jul-1994 (JP)
----------------
MODLOAD.C
MODP.PAS
MOD.ASM
        Added identification of signs xCHN, xxCH and TDZx.
        Removed unworking and unsupported FLT8.

MIDP.C
        Added saving of textmode and restoring it upon exit.



10-Jul-1994 (PK)
----------------
MMEM.H
MMEM.C
MIDAS.H
MODLOAD.C
S3MLOAD.C
MIDP.C
        Renamed MEM.H to MMEM.H to avoid the conflict with
        Borland C MEM.H. Also renamed MEM.C to MMEM.C to match
        the heared name.



11-Jul-1994 (PK)
----------------
EXAMPLES\EFFECTS.C
        Added a small example on how to do simple sound effects
        with MIDAS.



12-Jul-1994 (JP)
----------------
GUS.ASM
        Fixed a potentially dangerous bug in gusInit(), which
        caused the routine to write value 3 to all GUS registers
        0-31.



12-Jul-1994 (PK)
----------------
        MIDAS Sound System v0.41 revision b released.
