/*      SILLOAD.C
 *
 * Dummy Loader for an emtpy channel, v0.1
 * (Used when playing ONLY effects and no other sound!)
 *
 * Written in July 1994 by Stefan Engel (SE)
 *
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mtypes.h"
#include "errors.h"
#include "mglobals.h"
#include "mmem.h"
#include "sdevice.h"
#include "mplayer.h"
#include "silence.h"
#include "ems.h"
#include "vu.h"


/****************************************************************************\
\****************************************************************************/

char SONGNAME[] = "A song of SILENCE!!!";
char SIGN1[]    = "SILE";
char SIGN2[]    = "NCE!";

static mpModule *msil;



/****************************************************************************\
*
* Function:     int silFreeModule(mpModule *module, SoundDevice *SD);
*
* Description:  Deallocates a SILENCE module
*
* Input:        mpModule *module        module to be deallocated
*               SoundDevice *SD         Sound Device that has stored the
*                                       samples
*
* Returns:      MIDAS error code
*
\****************************************************************************/

int silFreeModule(mpModule *module, SoundDevice *SD)
{
    int error;

    if ( module == NULL )               /* valid module? */
    {
        ERROR(errUndefined, ID_silFreeModule);
        return errUndefined;
    }

    /* deallocate the module: */
    if ( (error = memFree(module)) != OK)
        PASSERROR(ID_silFreeModule)

    return OK;
}



/****************************************************************************\
*
* Function:     void silLoadError(SoundDevice *SD)
*
* Description:  Stops loading the module, deallocates all buffers and closes
*               the file.
*
* Input:        SoundDevice *SD         Sound Device that has been used for
*                                       loading.
*
\****************************************************************************/

static void silLoadError(SoundDevice *SD)
{
    /* Attempt to deallocate module if allocated. Do not process errors. */
    if ( msil != NULL )
        if ( silFreeModule(msil, SD) != OK )
            return;
}



/****************************************************************************\
*
* Function:     int silLoadModule(char *fileName, SoundDevice *SD,
*                   mpModule **module);
*
* Description:  Loads a SILENCE module into memory
*
* Input:        char *fileName          name of module file to be loaded
*               SoundDevice *SD         Sound Device which will store the
*                                       samples
*               mpModule **module       pointer to variable which will store
*                                       the module pointer.
*
* Returns:      MIDAS error code.
*               Pointer to module structure is stored in *module.
*
\****************************************************************************/

int silLoadModule(char *fileName, SoundDevice *SD, mpModule **module)
{
    silHeader       silh;
    int             error; /* MIDAS error code */
    

    /* point buffers to NULL so that modLoadError() can be
       called at any point: */
    msil = NULL;


    /* Allocate memory for the module structure: */
    if ( (error = memAlloc(sizeof(mpModule), (void**) &msil)) != OK )
    {
        silLoadError(SD);
        PASSERROR(ID_silLoadModule)
    }


    /*
     * We have no channles for sound !
     * If a MIDAS module player tries to access a SILENCE module
     * the player exits because no memory could be reserved!!!
     */
    msil->numChans = 0;                  /* store number of channels */

    memcpy(&msil->songName[0], &SONGNAME[0], 20);  /* copy song name */
    msil->songName[20] = 0;              /* force terminating '\0' */
    msil->songLength = 0;                /* copy song length */
    msil->numInsts = 0;                  /* set number of instruments */

    memcpy(&msil->ID, &SIGN1[0], 4); /* copy module signature */
    msil->IDnum = idSIL;                 /* SILENCE module */


    msil->numPatts = 0;   /* store number of tracks */

    *module = msil;                     /* return module ptr in *module */
    return OK;
}
