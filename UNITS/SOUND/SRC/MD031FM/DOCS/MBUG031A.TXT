        Bugs in MIDAS v0.31a and how to fix them
        ----------------------------------------

TASM: Constant too large
        It seems that TASM versions prior to 4.0 think that -64
        (here panLeft) is actually a 16 or even 32-bit number,
        and so using it with a 8-bit memory operand won't work.
        To fix these, check the lines which give you this error,
        and replace "panLeft" with "panLeft and 0FFh", if the
        other operand is 8-bit, or with "panLeft and 0FFFFh", if
        the other operand is 16-bit.

Sound Blaster 1.0 and 1.5 not working
        With real Creative Labs SB DSP 1.xx there seems to be a
        problem in turning on the speaker while sound is being
        played. The SB Sound Device does exactly this with mono
        single-cycle mode, as I experienced some problems with a
        PAS 16 without that. To fix the problem, comment out or
        remove line 883 in SB.ASM, which should read "SBCMD
        0D1h".

All of these will be naturally fixed in revision b, which will
also contain some example code on how to do sound effects with
MIDAS. It is actually very simple, but the lack of documentation
surely does not help here... Rev. b should be out in a few days
or so, depending whether I can manage to upload it from home to
Internet or not.

Petteri Kangaslampi, Alfred / S2
