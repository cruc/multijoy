#*      MIDP.MAK
#*
#* MIDAS Module Player makefile
#*
#* Copyright 1994 Petteri Kangaslampi and Jarno Paananen
#*
#* This file is part of the MIDAS Sound System, and may only be
#* used, modified and distributed under the terms of the MIDAS
#* Sound System license, LICENSE.TXT. By continuing to use,
#* modify or distribute this file you indicate that you have
#* read the license and understand and accept it fully.
#*


# Compiler directories:

BCDIR = d:\bcc
LIBDIR = $(BCDIR)\lib


# Compilers and options:

CC = bcc
CCOPTS = -c -3 -G -v -ml -l-v-c -I$(BCDIR)\include -DDEBUG -DREALVUMETERS
ASM = tasm
ASMOPTS = -UT310 -ml -zi -m9 -dDEBUG -dREALVUMETERS -dNOBORDERCOLOR
LINK = tlink
LINKOPTS = -c -v


# MIDAS Sound System object files:

MIDASOBJS = midas.obj mglobals.obj errors.obj mmem.obj ems.obj mod.obj modload.obj timer.obj gus.obj pas.obj wss.obj sb.obj nsnd.obj dsm.obj dma.obj vu.obj


midp.exe : midp.obj vgatext.obj $(MIDASOBJS)
        $(LINK) $(LINKOPTS) $(LIBDIR)\c0l.obj $(LIBDIR)\wildargs.obj midp.obj vgatext.obj @midasobj, midp, , $(LIBDIR)\cl.lib

midp.obj : midp.c midas.h
        $(CC) $(CCOPTS) midp.c

vgatext.obj : vgatext.asm lang.inc vgatext.inc
	$(ASM) $(ASMOPTS) vgatext.asm


mglobals.obj : mglobals.c
        $(CC) $(CCOPTS) mglobals.c

errors.obj : errors.c errors.h
        $(CC) $(CCOPTS) errors.c

midas.obj : midas.c midas.h
        $(CC) $(CCOPTS) midas.c

modload.obj : modload.c mtypes.h errors.h mglobals.h mmem.h sdevice.h \
        mplayer.h mod.h ems.h vu.h
        $(CC) $(CCOPTS) modload.c

mmem.obj : mmem.c mmem.h errors.h
        $(CC) $(CCOPTS) mmem.c


mod.obj : mod.asm lang.inc errors.inc mglobals.inc mod.inc mplayer.inc sdevice.inc ems.inc timer.inc mglobals.inc
        $(ASM) $(ASMOPTS) mod.asm

gus.obj : gus.asm lang.inc mglobals.inc sdevice.inc mem.inc mglobals.inc
        $(ASM) $(ASMOPTS) gus.asm

sb.obj : sb.asm lang.inc errors.inc sdevice.inc dsm.inc dma.inc
        $(ASM) $(ASMOPTS) sb.asm

wss.obj : wss.asm lang.inc errors.inc sdevice.inc dsm.inc dma.inc
        $(ASM) $(ASMOPTS) wss.asm

pas.obj : pas.asm pas.inc lang.inc errors.inc sdevice.inc dsm.inc dma.inc
        $(ASM) $(ASMOPTS) pas.asm

nsnd.obj : nsnd.asm lang.inc sdevice.inc
	$(ASM) $(ASMOPTS) nsnd.asm

dma.obj : dma.asm lang.inc errors.inc dma.inc mem.inc
	$(ASM) $(ASMOPTS) dma.asm

dsm.obj : dsm.asm lang.inc errors.inc mglobals.inc dsm.inc dma.inc mem.inc ems.inc sdevice.inc
	$(ASM) $(ASMOPTS) dsm.asm

timer.obj : timer.asm errors.inc mglobals.inc lang.inc timer.inc ems.inc dma.inc dsm.inc sdevice.inc
        $(ASM) $(ASMOPTS) timer.asm

ems.obj : ems.asm lang.inc errors.inc ems.inc mem.inc
	$(ASM) $(ASMOPTS) ems.asm

vu.obj : vu.asm lang.inc errors.inc vu.inc mem.inc sdevice.inc
        $(ASM) $(ASMOPTS) vu.asm




midas.h : errors.h mglobals.h mmem.h sdevice.h mplayer.h mod.h timer.h ems.h dma.h dsm.h vu.h
        touch midas.h

midas.inc : lang.inc errors.inc mglobals.inc mem.inc sdevice.inc mplayer.inc mod.inc timer.inc ems.inc dma.inc dsm.inc vu.inc
        touch midas.inc
