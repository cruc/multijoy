/*      SILENCE.H
 *
 * Dummy Player for an emtpy channel, v0.1
 * (Used when playing ONLY effects and no other sound!)
 *
 * Written in July 1994 by Stefan Engel (SE)
 *
*/

#ifndef __SIL_H
#define __SIL_H


/****************************************************************************\
*       struct modHeader
*       ----------------
* Description:  SILENCE module file header
\****************************************************************************/

typedef struct
{
    char        songName[21];            /* song name */
    char        sign1[4];                /* module signature */
    char        sign2[4];                /* module signature */
} silHeader;




extern ModulePlayer mpSIL;              /* Protracker Module Player */



#ifdef __cplusplus
extern "C" {
#endif


/****************************************************************************\
*
* Function:     int silLoadModule(char *fileName, SoundDevice *SD,
*                   mpModule **module);
*
* Description:  Loads a SILENCE module into memory
*
* Input:        char *fileName          name of module file to be loaded
*               SoundDevice *SD         Sound Device which will store the
*                                       samples
*               mpModule **module       pointer to variable which will store
*                                       the module pointer.
*
* Returns:      MIDAS error code.
*               Pointer to module structure is stored in *module.
*
\****************************************************************************/

int silLoadModule(char *fileName, SoundDevice *SD, mpModule **module);



/****************************************************************************\
*
* Function:     int modFreeModule(mpModule *module, SoundDevice *SD);
*
* Description:  Deallocates a SILENCE module
*
* Input:        mpModule *module        module to be deallocated
*               SoundDevice *SD         Sound Device that has stored the
*                                       samples
*
* Returns:      MIDAS error code
*
\****************************************************************************/

int silFreeModule(mpModule *module, SoundDevice *sd);


int silConvertSample(uchar *sample, ushort length);
int silConvertTrack(void *track, ushort type, ushort *trackLen);

int silIdentify(uchar *header, int *recognized);
int silInit(SoundDevice *SD);
int silClose(void);
int silPlayModule(mpModule *module, ushort firstSDChannel,
    ushort numSDChannels, ushort loopStart, ushort loopEnd);
int silStopModule(void);
int silSetInterrupt(void);
int silRemoveInterrupt(void);
int silPlay(void);
int silSetPosition(ushort pos);
int silGetInformation(mpInformation *info);



/****************************************************************************\
*       enum modFunctIDs
*       ----------------
* Description:  ID numbers for SILENCE Module Player functions
\****************************************************************************/

enum silFunctIDs
{
    ID_silIdentify = ID_sil,
    ID_silInit,
    ID_silClose,
    ID_silLoadModule,
    ID_silFreeModule,
    ID_silPlayModule,
    ID_silStopModule,
    ID_silSetInterrupt,
    ID_silRemoveInterrupt,
    ID_silPlay,
    ID_silSetPosition,
    ID_silGetInformation,
    ID_silConvertSample,
    ID_silConvertTrack
};



#ifdef __cplusplus
}
#endif


#endif
