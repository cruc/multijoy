unit VOCSOUND;


(* a Pascal interface to the Creative Technologies CT-VOICE.DRV *)
(* SoundBlaster digital sound driver                            *)

(* VOCSOUND adds VOC recording and saving abilities to VOCPLAY  *)


(* History

   VOCSOUND 0.93
     VOX file format introduced - consists of a lot of VOCs in a row
     use REWRITEVOX to open VOX file for writing
     use SAVEVOC2VOX for sequential write
     use CLOSEVOX to close VOX file
     displays version number if command line parameter '-D' is encountered
     saving now is possible even if VOCINSTALLED is FALSE

   VOCPLAY 0.9
     first functional version
*)
interface


uses vocplay;


function  savevoc   (voc : pvoc; vocfile : string) : boolean;
(* saves sample to a VOC file *)
(* extension is alway '.VOC'  *)
(* returns TRUE if successful *)


function  recordvoc (var voc : pvoc; size, frequency : word) : boolean;
(* records VOC file of size SIZE with sample rate FREQUENCY *)
(* legal frequency ranges from 4000 to 40000 Hz             *)
(* note: RECORDVOC turns off speaker to avoid feedbacks     *)
(* returns TRUE if successful, FALSE if out of memory       *)


function  rewritevox  (var voxfile : file; filename : string) : boolean;
(* creates/overwrites file <filename>.VOX *)
(* opens it for writing                   *)
(* returns TRUE if successful             *)


function  savevoc2vox (var voxfile : file; voc : pvoc) : boolean;
(* saves VOC sample to VOX file *)
(* returns TRUE if successful   *)


implementation


const version_number = '0.93';


function ctvoice1 (func_no, parameter : word) : word;
(* calls CTVOICE function FUNC_NO with PARAMETER in AX *)
(* returns value in AX                                 *)
var result : word;
begin
  asm
    mov  bx, func_no
    mov  ax, parameter
    call vocdriver
    mov  result, ax
  end;
  ctvoice1 := result;
end;


procedure ctvoice2 (func_no, par1, par2 : word);
(* calls CTVOICE function FUNC_NO with PAR1 in ES and PAR2 in DI *)
begin
  asm
    mov  bx, func_no
    mov  es, par1
    mov  di, par2
    call vocdriver
  end;
end;


function  savevoc   (voc : pvoc; vocfile : string) : boolean;
(* saves sample to a VOC file *)
(* extension is alway '.VOC'  *)
(* returns TRUE if successful *)
var f       : file;
    towrite : word;
    written : word;
    return  : word;
begin
  assign (f, vocfile + '.VOC');
  (*$I-*)
  rewrite (f, 1);
  (*$I+*)
  if ioresult <> 0 then begin
    savevoc := false;
    exit;
  end;

  (*$I-*)
  blockwrite (f, voc^.sample, voc^.length - 2);
  close (f);
  (*$I+*)

  if ioresult = 0 then savevoc := true
                  else savevoc := false;
end;


function recordvoc (var voc : pvoc; size, frequency : word) : boolean;
(* records VOC file of size SIZE with sample rate FREQUENCY *)
(* legal frequency ranges from 4000 to 40000 Hz             *)
(* note: RECORDVOC turns off speaker to avoid feedbacks     *)
(* returns TRUE if successful, FALSE if out of memory       *)
const headertext : array [0 .. headerlen - 1] of char =
      ('C','r','e','a','t','i','v','e',' ','V','o','i','c','e',' ',
       'F','i','l','e', #$1a, #$1a, #$00, #$0a, #$01, #$29, #$11);
var i       : integer;
    s,
    o       : word;
    sizehi,
    sizelo  : word;
    vocsize : word;
begin
  if not vocinstalled then begin
    recordvoc := false;
    exit;
  end;

  vocsize := size + headerlen + 2; (* +2 for storing LENGTH of a sample *)

  if (vocsize > maxvoc - 2) or (vocsize > maxavail) then begin
    recordvoc := false;
    exit;
  end;

  getmem (voc, vocsize);
  voc^.length := vocsize;
  for i := 0 to headerlen - 1 do voc^.sample [i] := ord (headertext [i]);

  if frequency <  4000 then frequency :=  4000;
  if frequency > 40000 then frequency := 40000;

  setspeaker (false); (* avoid feedbacks *)

  s := seg (voc^);
  o := ofs (voc^) + headerlen;
  sizehi := hi (size);
  sizelo := lo (size);;
  asm
    MOV  BX, 7
    MOV  AX, frequency
    MOV  DX, sizehi
    MOV  CX, sizelo
    MOV  ES, s
    MOV  DI, o
    CALL vocdriver
  end;

  recordvoc := true;
end;


function  rewritevox  (var voxfile : file; filename : string) : boolean;
(* creates/overwrites file <filename>.VOX *)
(* opens it for writing                   *)
(* returns TRUE if successful             *)
begin
  assign (voxfile, filename+'.VOX');
  {$I-}
  rewrite (voxfile, 1);
  {$i+}
  rewritevox := ioresult = 0;
end;


function  savevoc2vox (var voxfile : file; voc : pvoc) : boolean;
(* saves VOC sample to VOX file *)
(* returns TRUE if successful   *)
begin
  savevoc2vox := false;

  (*$I-*)
  blockwrite (voxfile, voc^.length, 2);
  if ioresult <> 0 then exit;
  blockwrite (voxfile, voc^.sample, voc^.length - 2);
  (*$I+*)
  if ioresult <> 0 then exit;

  savevoc2vox := true;
end;


var i : integer;
(* VOCSOUND *)
begin
  for i := 1 to paramcount do
    if (paramstr (i) = '-d') or (paramstr (i) = '-D') then begin
      writeln ('VOCSOUND ', version_number);
      break;
    end;
end.