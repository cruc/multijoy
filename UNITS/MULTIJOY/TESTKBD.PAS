program testkbd;

uses dos, crt;

(*$I MULTIKBD.INC*)

var i,
    j,
    k : integer;

begin
  clrscr;
  init_kbd;
  repeat
    k := 0;
    gotoxy (1, 1);
    for i := 0 to 15 do begin
     for j := 0 to 7 do begin
       if key [k] then write ('*')
                  else write ('-');
       if key [k] and (k = 42) then write (#7);
       inc (k);
     end;
     writeln;
    end;
  until key [1];
  reset_kbd;
end.