unit multijoy; (* reads out status of the MULTI JOYSTICK INTERFACE *)


(* MULTIJOY 2.0
   - second officially released version
   - supports 1- to 5-multiplexor interfaces with 4-bit address range
   - supports 3-button joysticks

   - new CFG file format, but still capable of reading the old CFG files
   - MULTIPATH now states the path to MULTIJOY.INI file
   - MULTIJOY.INI file contains former DOS environment variables
   - keyboard or additional interface on second printer port increases number
     of players

   - extensions:
     * PLAYERMAX is a constant and states theoretical maximum of players
     * MAXJOY    is a variable and states current maximum of joysticks
     * MAXPLAYER is, for compatibility reasons, still 6

   MULTIJOY 1.0
   - first officially released version
   - supports 2-multiplexor interfaces that use the BUSY and PE lines
*)


(* MULTIJOY uses the DOS environment variable:

   MULTIPATH, the path where the setup and config files are located.


   The setup file MULTIJOY.INI contains further variables:

   MULTICFG   is the name of the config file (without extension)


   Optional variables in MULTIJOY.INI are:

   MULTIPORT  is the printer port to be used (port 1 is the default).

   MULTIPORT2 is the printer port to be used for a second interface.

   MULTICFG2  is the name of the config file for the second interface.

   MULTIDELAY is the number of NOPs executed to wait for the joystick
              interface between writing and reading the printer port status.
              (MULTIDELAY is only necessary if you have
                a) a VERY fast computer or
                b) a VERY slow interface (i.e. a CMOS type)

   MULTIKBD   must be the name of a keyboard config file and makes it
              possible to play with two more players than joysticks.
              MULTIKBD is for additional players only, for keyboard emulation
              set MULTICFG to a keyboard file.
*)

interface


const maxplayer =  6;                  (* for compatibility reasons   *)
      playermax = 16;                  (* maximum number of players   *)


var   maxjoy    : integer;             (* actual number of joysticks  *)


type TJoy = record
              x, y         : shortint; (* -1 .. +1 (minus is left/up) *)
              lhit,  rhit,             (* directions triggered?       *)
              uhit,  dhit,             (* directions triggered?       *)
              khit,  xhit,
              zhit,  hhit,             (* buttons triggered?          *)
              knopf, xtra,
              zuper, hyper : boolean;  (* buttons held down?     *)
              (* (The HYPER button is, so far, only a theoretical *)
              (* possibility.)                                    *)
            end;

     TJoyState  = array [1 .. playermax] of TJoy;

     TJoyNum    = array [1 .. playermax] of byte;


var  JoyState   : TJoyState;
     (* contains all joystick status information *)
     (* is updated by calling GetAllJoyState     *)

     RealJoyNum,
     JoyNum     : TJoyNum;
     (* JoyNum [Player] contains physical joystick number of logical player *)

     MultiJoyInstalled : boolean;
     (* read-only, TRUE if MULTIJOY is active, FALSE otherwise      *)
     (* if installation is unsuccessful, MULTIJOY usually HALTs     *)
     (* exception: no DOS environment variables stated at all and   *)
     (* no MULTIJOY.INI file found in current directory.            *)
     (* If your program depends on MULTIJOY, it should halt with an *)
     (* appropriate error message if MULTIJOYINSTALLED is FALSE.    *)


procedure InitMultiJoy;
(* MULTIJOY initializes itself, but you can initialize again if you want *)

procedure GetAllJoyState;
(* gets status of all joystick ports *)

function  ChangeJoyNum : boolean;
(* changes joystick assignment defined by JoyNum                        *)
(* TRUE if no joystick is used more than once and 1 <= JoyNum <= MAXJOY *)

procedure ResetJoyNum;
(* resets joystick assignment and JoyNum array to initial status *)

procedure EmulationOn;
(* enables emulation (if config file contains keyword 'KEYBOARD') *)

procedure EmulationOff;
(* disables emulation (if enabled) *)


implementation


uses crt, dos;

(*$I MULTIERR.INC *)
(*$I MULTIKBD.INC *)

type TDecode  = record
                  line : array [1 .. 8] of record
                           stikno : byte;
                           action : char;
                         end;
                end;

const zero_action : TJoy
                  = (x     : 0    ; y    : 0;
                     lhit  : false; rhit : false;
                     uhit  : false; dhit : false;
                     khit  : false; xhit : false;
                     zhit  : false; hhit : false;
                     knopf : false; xtra : false;
                     zuper : false; hyper: false);
      actionchars = (['L', 'R', 'U', 'D', 'F', '*', 'S', 'H', '-']);

      line_mask   : array [0 .. 7] of byte
                  = ($01, $02, $04, $08, $10, $20, $40, $80);

var lines        : array [1 .. 2] of integer;    (* number of input bits *)
    devices,
    maxkey       : integer;
    line_assign  : array [1 .. 2, 1 .. 8] of record
                     mask   : byte;
                     invert : boolean;
                   end;
    delaycount   : word;
    pout,
    p_in,
    pout2,
    p_in2        : word;
    decode       : array [1 .. 2, 0 .. 15] of TDecode;
    key_number   : array [1 .. 2, 0 .. 6] of byte;
    PlayerNum    : TJoyNum;                    (* inversion of JoyNum *)
    hardware,
    keyboard     : boolean;


function ChangeJoyNum : boolean;
(* changes joystick assignment defined by JoyNum                        *)
(* TRUE if no joystick is used more than once and 1 <= JoyNum <= MAXJOY *)
var i    : integer;
    used : array [1 .. playermax] of boolean;
    good : boolean;
begin
  for i := 1 to maxjoy do begin
    joynum [i] := realjoynum [joynum [i]];
    used   [i] := false;
  end;
  realjoynum := joynum;

  changejoynum := true;
  for i := 1 to maxjoy do
    if not used [i] and (joynum [i] >= 1) and (joynum [i] <= maxjoy)
      then PlayerNum [JoyNum [i]] := i
      else changejoynum := false;
end;


procedure ResetJoyNum;
(* resets joystick assignment and JoyNum array to initial status *)
var i : integer;
begin
  for i := 1 to maxjoy do joynum [i] := i;
  realjoynum := joynum;
  changejoynum;
end;


procedure InitMultiJoy;
(* reads interface pin assignment from disk *)
(* resets joystick status variables         *)
var rline         : string;
    multipath     : string [80];
    multicfg,
    multicfg2,
    multikbd      : string  [8];
    multiport,
    multiport2    : string  [1];
    multidelay    : string  [5];
    printer_port,
    printer_port2 : byte;
    startplayer   : integer;
    old_cfg       : boolean;


  procedure error_msg (msg_nr, code : integer);
  (* calls MULTIERR error message procedure *)
  begin
    mj_error_msg (msg_nr, code, '', multipath);
  end;


  procedure cfg_error (msg_nr, code : integer; cfg_name : string);
  (* calls MULTIERR error message procedure *)
  begin
    mj_error_msg (msg_nr, code, cfg_name, multipath);
  end;


  function upstring (s : string) : string;
  (* returns string in which all lower case character of S are converted *)
  (* into upper case characters *)
  var i : integer;
  begin
    for i := 1 to length (s) do s [i] := upcase (s [i]);
    upstring := s;
  end;


  procedure getmultipath;
  (* reads MULTIPATH from DOS environment *)
  begin
    multipath := getenv ('MULTIPATH');
    if multipath <> '' then
      if multipath [length (multipath)] = '\' then
        multipath := copy (multipath, 1, length (multipath) - 1);
  end;


  procedure readsetupfile;
  (* reads file MULTIJOY.INI located at MULTIPATH for setup info *)


    function get_port_no (port_no : char) : byte;
    (* find printer port number in a string *)
    begin
      if not (port_no in ['1' .. '3']) then error_msg (8, ord (port_no));
      get_port_no := ord (port_no) - ord ('0');
    end;


  var setup    : text;
      error,
      equals   : integer;
      command  : string [20];
      valuestr : string [80];
      usesetup : boolean;
  (* readsetupfile *)
  begin
    multijoyinstalled := true;

    if multipath = '' then
      assign (setup, 'MULTIJOY.INI')
    else
      assign (setup, multipath + '\MULTIJOY.INI');

    (*$I-*)
    reset (setup);
    (*$I+*)
    error := ioresult;
    if error <> 0 then begin
      if multipath = '' then
        multijoyinstalled := false
      else begin
        usesetup   := false;
        multicfg   := getenv ('MULTICFG');
        if multicfg = '' then error_msg (14, error); (* setup file not found? *)
        multiport  := getenv ('MULTIPORT');
        multidelay := getenv ('MULTIDELAY');
      end;
    end else
      usesetup := true;

    multicfg2  := '';
    multiport2 := '';
    multikbd   := '';

    if usesetup then begin
      multicfg   := '';
      multiport  := '';
      multidelay := '';

      while not eof (setup) do begin
        readln (setup, rline);
        equals := pos ('=', rline);
        if equals > 0 then begin
          command  := upstring (copy (rline, 1, equals - 1));
          valuestr := upstring (copy (rline, equals + 1, length (rline)));
          if command = 'MULTICFG'   then multicfg   := valuestr else
          if command = 'MULTICFG2'  then multicfg2  := valuestr else
          if command = 'MULTIPORT'  then multiport  := valuestr else
          if command = 'MULTIPORT2' then multiport2 := valuestr else
          if command = 'MULTIDELAY' then multidelay := valuestr else
          if command = 'MULTIKBD'   then multikbd   := valuestr;
        end;
      end;
      close (setup);
    end;

    if multidelay <> '' then begin
      val (multidelay, delaycount, error);
      if error <> 0 then error_msg (13, 0); (* not a number? *)
      inc (delaycount);
    end else
      delaycount := 1;

    if multiport = ''  then printer_port := 1 (* default! *)
                       else printer_port := get_port_no (multiport [1]);

    pout := memw [$40:$8 + (printer_port - 1) * 2];
    if pout = 0 then error_msg (11, printer_port);
    p_in := pout + 1;

    if multiport2 <> '' then begin
      printer_port2 := get_port_no (multiport2 [1]);
      if printer_port2 = printer_port then error_msg (22, 0); (* port used twice? *)
      pout2 := memw [$40:$8 + (printer_port2 - 1) * 2];
      if pout2 = 0 then error_msg (11, printer_port2);
      p_in2 := pout2 + 1;
    end else
      if multicfg2 <> '' then error_msg (21, 0); (* undefined? *)

    if multicfg            = ''  then error_msg (6, 0); (* undefined? *)
    if pos ('.', multicfg) >  0  then error_msg (7, 0); (* extension? *)

  end;


  procedure readcfgfile (cfgname : string; keyflag : boolean);
  (* reads CFG file CFGNAME for information about the Multijoy hardware *)
  (* If KEYFLAG is set, only keyboard configs are allowed               *)
  var cfg     : text;
      rline   : string;
      line_no,
      error   : integer;
      command : string [20];


    procedure readnextline;
    (* reads next line from CFG (skipping empty lines) and upcases it *)
    var error : integer;
    begin
      repeat
        inc (line_no);
        if eof (cfg) then cfg_error (3, line_no, cfgname);
        (*$I-*)
        readln (cfg, rline);
        (*$I+*)
        error := ioresult;
        if error <> 0 then cfg_error (4, error, cfgname);
      until rline <> '';
      rline := upstring (rline);
    end;


    function nextword : string;
    (* parses string RLINE and returns next word specified by spaces *)
    (* RLINE is reduced to the so far not parsed rest                *)
    var p : integer;
    begin
      if rline <> '' then begin
        while rline [1] = ' ' do rline := copy (rline, 2, length (rline) - 1);
        p := pos (' ', rline);
        if p = 0 then p := length (rline) + 1;
        nextword := copy (rline, 1, p - 1);
        rline := copy (rline, p + 1, length (rline) - p);
      end else
        nextword := '';
    end;


    function value (no_string : string) : integer;
    (* returns integer value of string NO_STRING *)
    var result,
        error  : integer;
    begin
        val (no_string, result, error);
        if error <> 0 then cfg_error (17, line_no, cfgname);
        value := result;
    end;


    function firstchar (instring : string) : char;
    (* returns first character of string INSTRING *)
    begin
      firstchar := instring [1];
    end;


    procedure setoldjoylines;
    (* sets lines the joystick interface uses to PAPER EMTPY and BUSY that *)
    (* were used by the first release, two-multiplexor interfaces          *)
    begin
      inc (devices);
      lines [devices] := 2;
      with line_assign [devices, 1] do begin
        mask   := line_mask [5];
        invert := true;
      end;
      with line_assign [devices, 2] do begin
        mask   := line_mask [7];
        invert := false;
      end;
    end;


    procedure readjoylines;
    (* reads which lines a joystick interface uses *)
    begin
      inc (devices);
      command := nextword;
      if command = '' then cfg_error (16, line_no, cfgname); (* syntax? *)

      lines [devices] := 0;
      repeat
        inc (lines [devices]);
        with line_assign [devices, lines [devices]] do begin
          if command [1] = '!' then begin
            invert := false;
            command := copy (command, 2, length (command));
          end else
            invert := true;
          mask := line_mask [value (command)];
          command := nextword;
        end;
      until command = '';
    end;


    procedure readjoyaddresses;
    (* Which bit and address belongs to which joystick and action? *)
    var i,
        j : integer;
    begin
      maxjoy := startplayer;
      for i := 0 to 15 do begin
        with decode [devices, i] do begin
          readnextline;
          if value (nextword) <> i then cfg_error (16, line_no, cfgname); (* syntax? *)

          j := 1;
          command := nextword;
          while command <> '' do begin
            with line [j] do begin
              stikno := value (command) + startplayer;
              if stikno > maxjoy then maxjoy := stikno;
              if (stikno < 0) or (stikno > playermax) then
                cfg_error (12, stikno, cfgname);      (* stick out of range? *)
              action  := firstchar (nextword);
              if not (action in actionchars) then
                cfg_error (2, ord (action), cfgname); (* illegal action char? *)
            end;
            inc (j);
            command := nextword;
          end;
        end;
      end;
      hardware    := true;
      startplayer := maxjoy;
    end;


    procedure readkeyassignment (old : boolean);
    (* which key belongs to which player and action?     *)
    (* if OLD is TRUE, there are only 5 keys per player, *)
    (* otherwise there are 7 keys per player             *)
    var i,
        j : integer;
    begin
      if old then maxkey := 4
             else maxkey := 6;
      for i := 1 to 2 do
        for j := 0 to maxkey do begin
          readnextline;
          command := nextword;
          if not (command [1] in actionchars) then
            cfg_error (2, ord (command [1]), cfgname); (* illegal action char? *)
          key_number [i, j] := value (nextword);
        end;
      keyboard    := true;
      maxjoy      := startplayer + 2;
      startplayer := maxjoy;
    end;


  (* readcfgfile *)
  begin
    assign (cfg, multipath + '\' + cfgname + '.CFG');
    (*$I-*)
    reset(cfg);
    (*$I+*)
    error := ioresult;
    if error <> 0 then cfg_error (1, error, cfgname); (* file not found? *)

    line_no := 0;
    readnextline;
    old_cfg := (upstring (rline) <> 'MULTIJOY CONFIGURATION FILE');

    if not old_cfg then begin
      readnextline;
      if nextword <> 'MULTIJOY_VERSION:' then cfg_error (16, line_no, cfgname); (* syntax? *)
      (* future versions of MULTIJOY have to do version number checking here *)
      (* (version 2.0 uses the above magic words for this purpose)           *)

      readnextline;
      command := nextword;
      if command = 'LINES:'   then begin
        if keyflag then cfg_error (20, 0, cfgname);
        readjoylines;
        readjoyaddresses;
      end else if command = 'KEYBOARD' then begin
        if keyboard then cfg_error (19, 0, cfgname);
        readkeyassignment (false);
      end else
        cfg_error (16, line_no, cfgname); (* syntax? *)
    end else begin
      if upstring (rline) = 'KEYBOARD' then begin
        readkeyassignment (true);
      end else begin
        setoldjoylines;
        reset (cfg);
        line_no := 0;
        readjoyaddresses;
        if not eof (cfg) then begin
          readnextline;
          if nextword = 'INVERT' then begin
            line_assign [devices, 1].invert := false;
            line_assign [devices, 2].invert := true;
          end;
        end;
      end;
    end;

    close (cfg);
  end;


  procedure initjoysticks;
  (* initialize joystick status variables *)
  var i : integer;
  begin
    for i := 1 to maxjoy do begin
      RealJoyNum [i] := i;
      JoyNum     [i] := i;
      JoyState   [i] := zero_action;
    end;
    ChangeJoyNum;
  end;


(* InitMultiJoy *)
begin
  getmultipath;
  readsetupfile;
  if multijoyinstalled then begin
    startplayer := 0;
    devices     := 0;
    hardware := false;
    keyboard := false;
    readcfgfile (multicfg, false);
    if multicfg2 <> '' then readcfgfile (multicfg2, false);
    if multikbd  <> '' then readcfgfile (multikbd,  true );
    if keyboard then init_kbd;
    initjoysticks;
  end;
end;


procedure EmulationOn;
(* enables emulation (if config file contains keyword 'KEYBOARD') *)
begin
  if keyboard then init_kbd;
end;


procedure EmulationOff;
(* disables emulation (if enabled) *)
begin
  if keyboard then reset_kbd;
end;


procedure GetAllJoyState;
(* gets status of all joysticks *)
var address,
    joy,
    plr_now,
    player,
    pebu     : byte; (* PAPER EMPTY and BUSY bits *)
    old      : array [1 .. playermax] of record
                                           ox, oy     : shortint;
                                           ok, oe,
                                           oz, oh     : boolean;
                                          end;


  function read_port (mjoyaddress : byte; portno : integer) : byte;
  (* sets MULTI JOYSTICK INTERFACE to address MJOYADDRESS *)
  (* reads printer port, i.e. PAPER EMPTY and BUSY bits   *)
  begin
    if portno = 1 then port [pout ] := mjoyaddress
                  else port [pout2] := mjoyaddress;
    asm
      mov  cx, delaycount
    @l:
      nop
      loop @l
    end;
    if portno = 1 then read_port := port [p_in ]
                  else read_port := port [p_in2];
  end;


  procedure GetKeyState (key_no, joy : integer);
  (* gets status of joystick emulating keys *)
  begin
    with joystate [playernum [joy]] do
      with old [playernum [joy]] do begin
        if key [key_number [key_no, 0]] then begin
          x    := - 1;
          lhit := - 1 <> ox;
        end else
        if key [key_number [key_no, 1]] then begin
          x    :=   1;
          rhit :=   1 <> ox;
        end;
        if key [key_number [key_no, 2]] then begin
          y    := - 1;
          uhit := - 1 <> oy;
        end else
        if key [key_number [key_no, 3]] then begin
          y    :=   1;
          dhit :=   1 <> oy;
        end;
        if key [key_number [key_no, 4]] then begin
          knopf := true;
          khit  := not ok;
        end;
        if maxkey = 6 then begin
          if key [key_number [key_no, 5]] then begin
            xtra := true;
            xhit := not oe;
          end;
          if key [key_number [key_no, 6]] then begin
            zuper := true;
            zhit  := not oz;
          end;
        end;
      end;
  end;


  procedure decode_joystate (decode_action : char);
  (* decodes joystick status *)
  (* note: if you manage to invoke two opposite directions at the same *)
  (* time, the hit-indicators will be misleading                       *)
  begin
    with joystate [plr_now] do
      with old [plr_now] do begin
        case decode_action of
          'L' : begin x := - 1; lhit := - 1 <> ox;   end;
          'R' : begin x :=   1; rhit :=   1 <> ox;   end;
          'U' : begin y := - 1; uhit := - 1 <> oy;   end;
          'D' : begin y :=   1; dhit :=   1 <> oy;   end;
          'F' : begin knopf := true; khit := not ok; end;
          '*' : begin xtra  := true; xhit := not oe; end;
          'S' : begin zuper := true; zhit := not oz; end;
          'H' : begin hyper := true; hhit := not oh; end;
        end;
    end;
  end;


(* GetAllJoyState *)
var i,
    k : integer;
begin
  for joy := 1 to maxjoy do begin
    with JoyState [playernum [joy]] do
      with old [playernum [joy]] do begin
        ox := x;
        oy := y;
        ok := knopf;
        oe := xtra;
        oz := zuper;
        oh := hyper;
      end;
    JoyState [playernum [joy]] := zero_action;
  end;

  if multijoyinstalled then begin
    if hardware then begin
      for k := 1 to devices do
        for address := 0 to 15 do begin
          pebu := read_port (address, k);
          with decode [k, address] do
            for i := 1 to lines [k] do begin
              with line [i] do
                if stikno <> 0 then begin
                  plr_now := playernum [stikno];
                  with line_assign [k, i] do
                    if ((pebu and mask) = 0) xor invert then
                      decode_joystate (action);
                end;
            end;
        end;
      if keyboard then begin
        GetKeyState (1, maxjoy - 1);
        GetKeyState (2, maxjoy);
      end;
    end else
      if keyboard then begin
        GetKeyState (1, 1);
        GetKeyState (2, 2);
      end;
  end;
end;


(* MultiJoy *)
var i : integer;
begin
  for i := 1 to paramcount do
    if (paramstr (i) = '-d') or (paramstr (i) = '-D') then begin
      writeln ('MULTIJOY 2.0');
      break;
    end;
  InitMultiJoy;
end.