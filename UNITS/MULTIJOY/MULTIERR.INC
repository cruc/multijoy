(* error messages for use by MULTIJOY and MINIJOY *)


function dos_error (code : integer) : string;
(* returns dos error message to specified error code *)
var error : string;
begin
  case code of
     2 : error := ('File not found');
     3 : error := ('Path not found');
     5 : error := ('Access denied');
     6 : error := ('Invalid Handle');
     8 : error := ('Not enough memory');
    10 : error := ('Invalid environment');
    11 : error := ('Invalid format');
    18 : error := ('No more files');
    else begin
      str (code, error);
      dos_error := 'error #' + error;
    end;
  end;
  dos_error := error;
end;


procedure mj_error_msg (msg_nr, code : integer; cfg_name, multipath : string);
var setup : text;
    rline : string;
(* displays error message and halts the program *)
begin
  writeln ('MULTIJOY 2.0 error message: ');
  case msg_nr of
     5 : writeln ('DOS environment does not contain MULTIPATH (path of config file)');

    14 : begin
         writeln ('If you meant to use the DOS environment for setup: Your DOS environment');
         writeln ('does not contain MULTICFG (name of config file)');
         writeln ('If you meant to use a setup file: MULTIJOY.INI read error: ',  dos_error (code));
         end;
     6 : writeln ('MULTIJOY.INI/DOS environment does not contain MULTICFG  (name of config file)');
     7 : writeln ('MULTIJOY.INI/DOS environment variable MULTICFG must have no extension!');
    13 : writeln ('MULTIJOY.INI/DOS environment variable MULTIDELAY is not a number');
     8 : writeln ('Invalid MULTIJOY.INI variable MULTIPORT (''', chr (code), ''')');
    11 : writeln ('Printer port ', code, ' not found!');
    21 : writeln ('Secondary CFG file set but no printer port stated!');
    22 : writeln ('Primary and secondary printer port must not be identical!');

    15 : writeln ('Config file ''', cfg_name, ''' is bad or too old for MINIJOY!');
     1 : writeln ('Config file ''', cfg_name, ''' read error: ', dos_error (code));
    16 : writeln ('Config file ''', cfg_name, ''' syntax error in line ', code);
    17 : writeln ('Config file ''', cfg_name, ''' error in line ', code, ': number expected');
     2 : writeln ('Config file ''', cfg_name, ''' syntax error: unknown action code (''', chr (code), ''')');
     3 : writeln ('Config file ''', cfg_name, ''' read error: unexpected end of file in line ', code);
     4 : writeln ('Config file ''', cfg_name, ''' found but not able to read it (', dos_error (code), ')');
    12 : writeln ('Config file ''', cfg_name, ''' error: illegal joystick number (', code, ')!');
    10 : writeln ('Config file ''', cfg_name, ''' read error: address out of range (''', code, ''')');

    20 : writeln ('MULTIKBD has to be a keyboard config file');
    19 : writeln ('Can''t use MULTIKBD because MULTICFG already uses the keyboard!');
    18 : writeln ('MULTIKBD file not found!');

     9 : writeln ('MINIJOY does not support keyboard emulation mode!');

    else writeln ('Critical error - no appropriate error message found (error #', code, ')');
  end;
  writeln;
  if msg_nr <> 5 then begin
    writeln ('MULTIPATH = ', multipath);
    assign (setup, multipath + '\MULTIJOY.INI');
    (*$I-*)
    reset (setup);
    if ioresult = 0 then
      writeln;
      while not eof (setup) do begin
        readln (setup, rline);
        writeln (rline);
      end;
    (*$I+*)
  end;

  halt;
end;