program helpdemo;


(* demonstrates use of SHOWHELP unit *)


uses showhelp,
     scorescr,
     fastgraf,
     multijoy;


var noscore  : array [1 .. maxplayer] of tplayerstat;
    demopal  : tpalette;


procedure show_help; far;
(* dummy procedure to make far call possible *)
begin
  showhelp.show_help;
end;


procedure start_graphics;
(* inititializes 640x480 mode and loads palette *)
begin
  initgraph (g320x200);
  loadpalette (@demopal, 'helpdemo');
  setallpalette (@demopal);
  helppal := @demopal;
end;


procedure customize_scorescr;
(* initialize SCORESCR environment *)
var i : integer;
begin
  with env do begin
    palette      := demopal;
    numpagesused := 1;
    alwaysfade   := false;
    teamplay     := true;
    gametitle    := 'HELPDEMO';
    helptext     := '   H HELP!';
    keyfunction  ['h'] := show_help;
    keynewscreen ['h'] := true;
    for i := 1 to maxplayer do begin
      playerstats [i] := @noscore [i].score;
      initplayerstats (playerstats [i]^);
    end;
  end;
end;


begin
  start_graphics;
  customize_scorescr;
  load_help ('TANKHELP');
  helpcol := 142;
  repeat
  until scorescreen (true) = #27;
  free_help;
  closegraph;
end.