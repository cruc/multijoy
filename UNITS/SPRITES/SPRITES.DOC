  sprite machine for multijoy games  --  copyright (c) 1994 by Henning Katte

  In  most  games a lot of code is needed to control and display movable and
  animated bitmaps, so called sprites. Especially if the technique of double
  buffering is applied, the sprite handling is a non-trivial job. The sprite
  machine  offers this functionality to bitmap based games and liberates the
  programmer  from  unpleasant  things  such  as double buffering, collision
  detection, color masking and the difficult adaptation to different monitor
  frequencies.  Moreover, it realizes the idea of virtual positioning.  That
  means,  sprites  can  be placed independently of the screen resolution and
  bounds.

  The unit is implemented in object orientated pascal, so you can easily add
  functionality by deriving the base classes. The most important object type
  is TSprite, which is the basis for all sprites.  A specialized  descendant
  is  TAnimation.  It is used for animated sequences like explosions,  smoke
  clouds and so on.

  The created sprites are organized in layers (instances of TLayer) in order
  to realize a classification (e.g. player and monster sprites should belong
  to different layers) and a drawing order (clouds should appear uppermost).
  Use  TLayer.Insert  or TLayer.InsertAt in order to assign a created sprite
  to a sprite layer. Normally, you do not have to delete those sprites your-
  self, the layer destructor will do this for you, calling TLayer.Delete for
  each sprite in the layer.

  The  layers  itselves  are  attached to the sprite handler (an instance of
  THandler),  which clears the sprites from the screen, moves them and draws
  the  sprites  at  their new positions by a single call of THandler.Animate
  (to be exact: the handler initiates its layers to do so). Before that, you
  can call THandler.TogglePage in your main loop to toggle the active screen
  page and wait for the next screen refresh.  Furthermore, you can overwrite
  THandler.Idle  in  order to do something while the sprites are not visible
  on the screen (the sprites are not moved at this point).  But don't forget
  to call the inherited method!

  The  sprite  handler is also capable of managing bitmaps that stick to the
  screen  after  creation, so called static objects.  The sprite unit offers
  object  types  for text, centered text, numbers, bitmaps, shapes (that are
  masked bitmaps) and sprites.  Further types can easily be derived from the
  base  class  TStatic or one of its descendants.  Use THandler.PutStatic to
  assign an instance to the handler. Calls of THandler.Animate will draw the
  queue contents on the current active, thus non-visual, screen page.  After
  a  static  object is displayed on all screen pages, it is disposed by  the
  sprite handler. See THandler.Idle for details.

  When constructing a sprite, you can specify a color mask. This is a 32 bit
  vector,  that  determines  which colors appear below and above the sprite.
  Each bit stands for eight colors, so the first bit (least significant bit)
  represents  colors 0 to 7, the second colors 8 to 15 and so on.  A cleared
  bit means that the sprite covers the corresponding colors.

  If you need a new class of sprites in your game, you normally just have to
  derive  TSprite  and overwrite its virtual method Move, which is called by
  THandler.Animate for each sprite.  You can use MoveTo and MoveRel in order
  to  change  the position of the sprite absolute or relative on the virtual
  screen. Do not alter the object fields themselves, as the change would not
  affect the real screen position, unless you call the Translate method.

  (speed adaptation)

  THandler.Animate measures the current frequency, that depends on the
  vertical monitor frequency and the number of screen refreshes, that are
  used by the game.

  By default, the method TSprite.Put calls PutSprite in the unit Fastgraf to
  display  the sprite image on the screen.  If you don't need color masking,
  you  can overwrite this method and use PutShape or PutShapeAdd instead, in
  order to improve the graphics performance. TSprite's methods Get and Clear
  normally  call  Fastgraf's  GetBitmap  and PutBitmap to get and redraw the
  sprite's background.  Overwrite these methods, if background saving is not
  required.

  (collision control)

  The  sprite  machine  offers a profile mode to games that use the vertical
  screen  refresh for timing.  In development, you can use this feature as a
  load indicator. If enabled, the profile mode can be switched on and off by
  the scroll lock key.  Normally, you will then notice a colored beam at the
  left and right side of your display.  If the beam reaches to the bottom of
  the screen then your machine is almost exhausted. The different colors re-
  present the different animation phases:  clear phase is red, move phase is
  green  and put phase is blue.  The idle and remove phases do not take much
  time, so they are not indicated individually.
