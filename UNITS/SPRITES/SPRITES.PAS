unit Sprites;

{****************************************************************************
 *                                                                          *
 * unit SPRITES -- sprite machine for multijoy games                        *
 *                                                                          *
 * copyright (c) 1994 by Henning Katte -- all rights reserved               *
 *                                                                          *
 * thanks to Christof Ruch for his suggestions and bug reports              *
 * read .doc file for introduction                                          *
 * refer to program Aquarium for demonstration                              *
 *                                                                          *
 * version 1.55, 17/08/94                                                   *
 *                                                                          *
 ****************************************************************************
 *                                                                          *
 * version 0.90 (first official version)                                    *
 *                                                                          *
 * version 0.95                                                             *
 *   added static text objects (TStaticText, TStaticCText, TStaticNum)      *
 *   sprites no longer dispose themselves (a very bad idea!)                *
 * ! the virtual coordinates are now long integers                          *
 * ! using 1:15:16 bit fixpoint format (Res now equals 65536)               *
 *   added version output to initialization part                            *
 *                                                                          *
 * version 1.0                                                              *
 *   killed bug in TAnimation.Move (pixel trash)                            *
 * ! profile mode must be activated by scroll lock if enabled               *
 *   enhanced SpriteGet (now gets background only if necessary)             *
 * ! buffer sizes for animations are now calculated automatically           *
 *                                                                          *
 * version 1.1                                                              *
 *   fixed bug in TSprite.BoundsColl                                        *
 * ! new method TSprite.OutOfBounds                                         *
 *   profile mode now shows only clear, move and put phases                 *
 *   new writable constant ProfCol                                          *
 * ! new option flag ResetPos for animations                                *
 *   killed bug in TAnimation (sprite moved in delay phase)                 *
 *                                                                          *
 * version 1.2                                                              *
 * ! renamed TSprite.Flicker to TSprite.Blink                               *
 *   new global variable Profile (indicates active profile mode)            *
 *   new constants ProfileOn and ProfileOff for use in THandler.Init        *
 *   background color is now restored in profile mode                       *
 *   fixed bug in TStaticNum (used y instead of x coordinate)               *
 * ! new constants NoBuffer and AutoSize for use in TSprite.Init            *
 *   new method TSprite.Remap                                               *
 *                                                                          *
 * version 1.5                                                              *
 * ! included automatic speed and delay adjustment                          *
 * ! new method TSprite.MoveAdj for adjusted movements (replaces MoveRel)   *
 *   delay values are now in real time (millisecs), new constant Clock      *
 *   deleted global variable Frames, instead new variable Time              *                                         *
 *   moved most object fields into private scopes                           *
 *   new methods TLayer.Number, TLayer.Head, TLayer.Next, TLayer.At         *
 *   Count in THandler is now the number of layers, not one below           *
 *   new method TStatic.Valid, that decrements the internal counter         *                                               *
 *   new type Fix, representing the fix point type                          *
 *                                                                          *
 * version 1.55                                                             *
 *   returned to frame based instead of real time delay values              *
 *   renamed global variable Freq to CurFreq (current frequency)            *
 *   RefFreq (reference frequency) is now a global variable                 *
 *   new global variable Frames (fixpoint)                                  *                                             *
 *   Time is now measured in seconds (fixpoint)                             *
 *                                                                          *
 ****************************************************************************}

{$A+,B-,E-,F-,G+,I-,N-,O-,P-,T-,V-,X+}

interface

uses
  Fastgraf,
  Fasttext,
  Protim;

type
  Fix         = longint;
  Vector      = longint;

const
  Name        = 'SPRITES';
  Version     = '1.55';

  Res         = 65536;

  ColorGroups = 32;

  MaxSteps    = 256;
  MaxSprites  = 256;
  MaxLayers   = 16;
  MaxStatics  = 256;

  NoBuffer    = 0;
  AutoSize    = $FFFF;

  ProfileOn   = true;
  ProfileOff  = false;

  Once        = 1;
  Infinite    = -1;

  BottomUp    = 0;
  TopDown     = 1;

  CenterX     = 1;
  CenterY     = 2;
  Centered    = CenterX + CenterY;
  ResetPos    = 4;

  sfActive    = 1;
  sfPage0     = 2;
  sfPage1     = 4;
  sfVisible   = sfPage0 + sfPage1;
  sfHidden    = 8;
  sfBlink     = 16;

const
  ProfCol : byte = 0;

var
  Curr    : byte;
  Last    : byte;

  Time    : Fix;
  Frames  : Fix;

  RefFreq : integer;
  CurFreq : integer;
  Adjust  : Fix;

  Refresh : boolean;

  Profile : boolean;

type
  TColor = array[0..ColorGroups - 1] of word;

  TGround = record
    Mask   : Vector;
    Pixel  : word;
    Color  : TColor;
    Bound  : TColor
  end;

  TBuffer = record
    XPos   : integer;
    YPos   : integer;
    XSize  : integer;
    YSize  : integer;
    Bitmap : PBitmap;
  end;

  PSprite = ^TSprite;
  TSprite = object
    Options : word;
    State   : word;
    Image   : PBitmap;
    HSize   : integer;
    VSize   : integer;
    Mask    : Vector;
    Coll    : Vector;
    Offset  : byte;
    XVir    : Fix;
    YVir    : Fix;
    XScr    : integer;
    YScr    : integer;
    Ground  : TGround;
    ImgSize : word;
    MemSize : word;
    Buffer  : array[0..1] of TBuffer;
    constructor Init(AImage : PBitmap; AMask, AColl : Vector;
                     AOffset : byte; AXVir, AYVir : Fix;
                     ADelay, AMemSize, AOptions : word);
    destructor Done; virtual;
    procedure Remap(AMask : Vector; AOffset : byte);
    procedure NewImage(AImage : PBitmap);
    procedure Translate;
    procedure Hide;
    procedure Show;
    procedure Toggle;
    procedure Remove;
    procedure Blink(Num : integer; Start, ShowDelay, HideDelay : word);
    procedure MoveTo(AXVir, AYVir : Fix);
    procedure MoveRel(XRel, YRel : Fix);
    procedure MoveAdj(XRel, YRel : Fix);
    procedure SpriteClear;
    procedure SpriteGet;
    procedure SpritePut;
    procedure SpriteMove;
    procedure Clear; virtual;
    procedure Get; virtual;
    procedure Put; virtual;
    procedure Move; virtual;
    function OutOfBounds : boolean; virtual;
    function BoundsColl : boolean; virtual;
    function ScreenColl : boolean; virtual;
    function GroundColl : boolean; virtual;
    function SpriteColl(Sprite : PSprite) : boolean; virtual;
  private
    Delay    : word;
    ShowDel  : word;
    HideDel  : word;
    BlinkCnt : integer;
  end;

  PMap = ^TMap;
  TMap = array[0..MaxSteps - 1] of PBitmap;

  PAnim = ^TAnim;
  TAnim = array[0..MaxSteps - 1, 0..1] of word;

  PAnimation = ^TAnimation;
  TAnimation = object(TSprite)
    Loops   : integer;
    Num     : word;
    Map     : PMap;
    Anim    : PAnim;
    Step    : word;
    Counter : word;
    XOff    : Fix;
    YOff    : Fix;
    DX      : Fix;
    DY      : Fix;
    constructor Init(ALoops: integer; ANum : word;
                     AMap : PMap; AAnim : PAnim;
                     AMask : Vector; AOffset : byte;
                     AXVir, AYVir, ADX, ADY : Fix;
                     ADelay, AOptions : word);
    procedure Move; virtual;
  end;

  PLayer = ^TLayer;
  TLayer = object
    constructor Init(ADir : byte);
    destructor Done;
    function Number : integer;
    function Head : PSprite;
    function Next : PSprite;
    function At(Pos : integer) : PSprite;
    function IndexOf(ASprite : PSprite) : integer;
    function InsertAt(ASprite : PSprite; Pos : integer) : integer;
    function Insert(ASprite : PSprite) : integer;
    function DeleteAt(Pos : integer) : integer;
    function Delete(ASprite : PSprite) : integer;
    procedure Clear;
    procedure Get;
    procedure Move;
    procedure Put;
    procedure Remove;
    function SpriteColl(Sprite : PSprite) : PSprite;
  private
    Dir    : byte;
    Count  : integer;
    First  : integer;
    Last   : integer;
    Index  : integer;
    Sprite : array[0..MaxSprites - 1] of PSprite;
    procedure Free(ASprite : PSprite);
  end;

  PStatic = ^TStatic;
  TStatic = object
    constructor Init(AX, AY : integer);
    destructor Done; virtual;
    procedure Draw; virtual;
    function Valid: boolean;
  private
    Count : byte;
    X, Y  : integer;
  end;

  PStaticText = ^TStaticText;
  TStaticText = object(TStatic)
    constructor Init(AX, AY : integer; const AText : string);
    procedure Draw; virtual;
  private
    Text : string;
  end;

  PStaticCText = ^TStaticCText;
  TStaticCText = object(TStaticText)
    procedure Draw; virtual;
  end;

  PStaticNum = ^TStaticNum;
  TStaticNum = object(TStatic)
    constructor Init(AX, AY : integer; AN, AS : longint);
    procedure Draw; virtual;
  private
    N, S : longint;
  end;

  PStaticBitmap = ^TStaticBitmap;
  TStaticBitmap = object(TStatic)
    constructor Init(AX, AY : integer; ABitmap : PBitmap);
    procedure Draw; virtual;
  private
    Bitmap : PBitmap;
  end;

  PStaticShape = ^TStaticShape;
  TStaticShape = object(TStaticBitmap)
    procedure Draw; virtual;
  end;

  PStaticSprite = ^TStaticSprite;
  TStaticSprite = object(TStaticBitmap)
    constructor Init(AX, AY : integer; ABitmap : PBitmap;
                     AMask : Vector; AOffset : byte);
    procedure Draw; virtual;
  private
    Mask   : Vector;
    Offset : byte;
  end;

  TStack = record
    Num    : integer;
    Static : array[0..MaxStatics - 1] of PStatic
  end;

  PHandler = ^THandler;
  THandler = object
    constructor Init(ARefFreq, AMaxFrames : integer; AEnable : boolean);
    destructor Done; virtual;
    procedure Assign(ALayer : PLayer);
    procedure Animate;
    procedure TogglePage;
    procedure PutStatic(AStatic : PStatic);
    procedure Idle; virtual;
  private
    MaxFrames : integer;
    Enable    : boolean;
    FrameTime : longint;
    StartTime : longint;
    Count     : integer;
    Layer     : array[0..MaxLayers - 1] of PLayer;
    Stack     : array[0..1] of TStack;
  end;

{$L SPRITES.OBJ}

implementation

uses
  Crt, Dos;

{****************************************************************************
 *                                                                          *
 *                          methods of TSprite                              *
 *                                                                          *
 ****************************************************************************}

constructor TSprite.Init(AImage : PBitmap; AMask, AColl : Vector;
                         AOffset : byte; AXVir, AYVir : Fix;
                         ADelay, AMemSize, AOptions : word);
var
  I, J : byte;
begin
  Coll     := AColl;
  XVir     := AXVir;
  YVir     := AYVir;
  Options  := AOptions;
  BlinkCnt := 0;

{ calc delay }

  Delay := (ADelay * CurFreq) div RefFreq;

{ remap colors }

  Remap(AMask, AOffset);

{ calculate buffer size }

  if AMemSize <> AutoSize then
    MemSize := AMemSize
  else
    if AImage = nil then
      MemSize := NoBuffer
    else
      MemSize := BitmapSize(AImage^.B, AImage^.H);

{ assign image bitmap }

  NewImage(AImage);
  Translate;

  if Delay = 0 then
    State := sfActive
  else
    State := sfActive + sfHidden;

{ clear Ground record }

  with Ground do
  begin
    Mask  := 0;
    Pixel := 0;
    FillChar(Color, ColorGroups shl 1, 0);
    FillChar(Bound, ColorGroups shl 1, 0)
  end;

{ initialize double buffering }

  for I := 0 to Last do
    with Buffer[I] do
    begin
      XSize := 0;
      YSize := 0;
      if MemSize = NoBuffer then
        Bitmap := nil
      else
      begin
        GetMem(Bitmap, MemSize);
        if Bitmap = nil then
        begin
          for J := 0 to Last - 1 do
            FreeMem(Buffer[J].Bitmap, MemSize);
          Fail
        end
      end
    end
end;

destructor TSprite.Done;
var
  I : byte;
begin
  if MemSize > 0 then
    for I := 0 to Last do
      FreeMem(Buffer[I].Bitmap, MemSize)
end;

procedure TSprite.Remap; external;

procedure TSprite.NewImage; external;

procedure TSprite.Translate; external;

procedure TSprite.Hide; external;

procedure TSprite.Show; external;

procedure TSprite.Toggle; external;

procedure TSprite.Remove; external;

procedure TSprite.Blink; external;

procedure TSprite.MoveTo; external;

procedure TSprite.MoveRel; external;

procedure TSprite.MoveAdj; external;

procedure TSprite.SpriteClear; external;

procedure TSprite.SpriteGet; external;

procedure TSprite.SpritePut; external;

procedure TSprite.SpriteMove; external;

procedure TSprite.Clear;
begin
  if MemSize <> NoBuffer then
    with Buffer[Curr] do
      PutBitmap(XPos, YPos, Bitmap);
end;

procedure TSprite.Get;
begin
  if MemSize <> NoBuffer then
    GetBitmap(XScr, YScr, HSize, VSize, Buffer[Curr].Bitmap)
end;

procedure TSprite.Put;
begin
  if MemSize <> NoBuffer then
    PutSprite(XScr, YScr, Offset, Mask, Image, Buffer[Curr].Bitmap)
end;

procedure TSprite.Move;
begin
end;

function TSprite.OutOfBounds; external;

function TSprite.BoundsColl; external;

function TSprite.ScreenColl; external;

function TSprite.GroundColl; external;

function TSprite.SpriteColl; external;

{****************************************************************************
 *                                                                          *
 *                          methods of TAnimation                           *
 *                                                                          *
 ****************************************************************************}

constructor TAnimation.Init(ALoops : integer; ANum : word;
                            AMap : PMap; AAnim : PAnim;
                            AMask : Vector; AOffset : byte;
                            AXVir, AYVir, ADX, ADY : Fix;
                            ADelay, AOptions : word);
var
  I, AMemSize, ASize : word;
begin

{ calculate buffer size }

  AMemSize := 0;
  for I := 0 to ANum - 1 do
    with AMap^[AAnim^[I, 0]]^ do begin
      ASize := BitmapSize(B, H);
      if ASize > AMemSize then
        AMemSize := ASize
    end;

  inherited Init(nil, AMask, 0, AOffset, AXVir, AYVir,
                 ADelay, AMemSize, AOptions);

  Loops   := ALoops;
  Num     := ANum;
  Map     := AMap;
  Anim    := AAnim;
  XOff    := AXVir;
  YOff    := AYVir;
  DX      := ADX;
  DY      := ADY;
  Step    := Num;
  Counter := 0;
end;

procedure TAnimation.Move; external;

{****************************************************************************
 *                                                                          *
 *                          methods of TLayer                               *
 *                                                                          *
 ****************************************************************************}

constructor TLayer.Init(ADir : byte);
begin
  Dir   := ADir;
  Count := 0;
  First := MaxSprites;
  Last  := -1;
  Index := MaxSprites;
  FillChar(Sprite, MaxSprites * SizeOf(pointer), 0)
end;

destructor TLayer.Done;
var
  I : integer;
begin
  for I := First to Last do
    if Sprite[I] <> nil then
      Free(Sprite[I]);
end;

function TLayer.Number; external;

function TLayer.Head; external;

function TLayer.Next; external;

function TLayer.At; external;

function TLayer.IndexOf; external;

function TLayer.InsertAt; external;

function TLayer.Insert; external;

function TLayer.DeleteAt; external;

function TLayer.Delete; external;

procedure TLayer.Clear; external;

procedure TLayer.Get; external;

procedure TLayer.Move; external;

procedure TLayer.Put; external;

procedure TLayer.Remove; external;

function TLayer.SpriteColl; external;

procedure TLayer.Free(ASprite : PSprite);
begin
  Dispose(ASprite, Done);
end;

{****************************************************************************
 *                                                                          *
 *                          methods of TStatic                              *
 *                                                                          *
 ****************************************************************************}

constructor TStatic.Init(AX, AY : integer);
begin
  Count := Last + 1;
  X := AX;
  Y := AY;
end;

destructor TStatic.Done;
begin
end;

procedure TStatic.Draw;
begin
end;

function TStatic.Valid : boolean;
begin
  if Count = 0 then
    Valid := false
  else
  begin
    Valid := true;
    Dec(Count);
  end
end;

{****************************************************************************
 *                                                                          *
 *                          methods of TStaticText                          *
 *                                                                          *
 ****************************************************************************}

constructor TStaticText.Init(AX, AY : integer; const AText : string);
begin
  inherited Init(AX, AY);
  Text := AText;
end;

procedure TStaticText.Draw;
begin
  PutText(X, Y, Text);
end;

{****************************************************************************
 *                                                                          *
 *                          methods of TStaticMText                         *
 *                                                                          *
 ****************************************************************************}

procedure TStaticCText.Draw;
begin
  PutTextM(X, Y, Text);
end;

{****************************************************************************
 *                                                                          *
 *                          methods of TStaticNum                           *
 *                                                                          *
 ****************************************************************************}

constructor TStaticNum.Init(AX, AY : integer; AN, AS : longint);
begin
  inherited Init(AX, AY);
  N := AN;
  S := AS;
end;

procedure TStaticNum.Draw;
begin
  PutNum(X, Y, N, S);
end;

{****************************************************************************
 *                                                                          *
 *                          methods of TStaticBitmap                        *
 *                                                                          *
 ****************************************************************************}

constructor TStaticBitmap.Init(AX, AY : integer; ABitmap : PBitmap);
begin
  inherited Init(AX, AY);
  X := AX;
  Y := AY;
  Bitmap := ABitmap
end;

procedure TStaticBitmap.Draw;
begin
  if Bitmap <> nil then
    PutBitmap(X, Y, Bitmap)
end;

{****************************************************************************
 *                                                                          *
 *                          methods of TStaticShape                         *
 *                                                                          *
 ****************************************************************************}

procedure TStaticShape.Draw;
begin
  if Bitmap <> nil then
    PutShape(X, Y, Bitmap)
end;

{****************************************************************************
 *                                                                          *
 *                          methods of TStaticSprite                        *
 *                                                                          *
 ****************************************************************************}

constructor TStaticSprite.Init(AX, AY : integer;
                               ABitmap : PBitmap; AMask : Vector;
                               AOffset : byte);
begin
  inherited Init(AX, AY, ABitmap);
  Mask   := AMask;
  Offset := AOffset
end;

procedure TStaticSprite.Draw;
var
  Buffer : PBitmap;
begin
  if Bitmap <> nil then
    with Bitmap^ do
    begin
      Buffer := AllocateBitmap(B, H);
      GetBitmap(X, Y, B, H, Buffer);
      PutSprite(X, Y, Offset, Mask, Bitmap, Buffer);
      FreeBitmap(Buffer)
    end
end;

{****************************************************************************
 *                                                                          *
 *                          methods of THandler                             *
 *                                                                          *
 ****************************************************************************}

constructor THandler.Init(ARefFreq, AMaxFrames : integer; AEnable : boolean);
var
  I : byte;
begin
  RefFreq   := ARefFreq;
  MaxFrames := AMaxFrames;
  Enable    := AEnable;
  Time      := 0;
  Frames    := 0;
  Adjust    := Res;
  StartTime := 0;
  Count     := 0;

{ determine last screen page }

  Curr      := 0;
  if NumPages > 1 then
    Last := 1
  else
    Last := 0;

{ clear static object stack }

  for I := 0 to Last do
    Stack[I].Num := -1;

{ determine monitor frequency }

(*  PT_StartTimer;*)
  WaitForRetrace;
  WaitForDisplay; (* CHRISTOFS KLUDGE *)
  StartTime := PT_ReadTimer;
  WaitForRetrace;
  FrameTime := (PT_ReadTimer - StartTime);
  CurFreq := Round(1 / FrameTime * TFreq);
end;

destructor THandler.Done;
var
  I : byte;
  J : integer;
begin
  for I := 0 to Last do
    with Stack[I] do
      for J := 0 to Num do
        Dispose(Static[J], Done)
end;

procedure THandler.Assign(ALayer : PLayer);
begin
  if (ALayer <> nil) and (Count < MaxLayers) then
  begin
    Layer[Count] := ALayer;
    Inc(Count);
  end
end;

procedure THandler.Animate;

var
 AR, AG, AB : byte;

procedure CalcAdjust;
var
  StopTime  : longint;
  NumFrames : real;
begin
  StopTime  := PT_ReadTimer;
  NumFrames := (StopTime - StartTime) / FrameTime;
  if (NumFrames > 0) and (NumFrames <= MaxFrames) then
  begin
    CurFreq := Round(1 / FrameTime * TFreq / NumFrames);
    Adjust  := Round((RefFreq / CurFreq) * Res);
    Inc(Time, Round((StopTime - StartTime) / TFreq * Res));
    Inc(Frames, Adjust);
  end;
  StartTime := StopTime
end;

procedure CheckProfile;
var
  R : Registers;
begin
  R.AH := $02;
  Intr($16,R);
  Profile := Enable and ((R.AL and $10) <> 0);
end;

procedure GetBKColor;
begin
  if Profile then
    GetRGBPalette(ProfCol, @AR, @AG, @AB);
end;

procedure SetBkColor(R, G, B : byte);
begin
  if Profile then
    SetRGBPalette(ProfCol, R, G, B);
end;

procedure RestoreBkColor;
begin
  if Profile then
    SetRGBPalette(ProfCol, AR, AG, AB);
end;

var
  I : integer;
begin
  Refresh := false;

  { calculate adjustment and check profile mode }

  CalcAdjust;
  CheckProfile;
  GetBkColor;

  { clear all sprites and draw static objects (RED) }

  SetBkColor(63, 0, 0);
  for I := 0 to Count - 1 do
    Layer[I]^.Clear;
  Idle;

  { move all sprites and get backgrounds (GREEN) }

  SetBkColor(0, 63, 0);
  for I := 0 to Count - 1 do
    Layer[I]^.Move;
  for I := 0 to Count - 1 do
    Layer[I]^.Get;

  { put all sprites (BLUE) }

  SetBkColor(0, 0, 63);
  for I := 0 to Count - 1 do
    Layer[I]^.Put;

  { dispose sprites to be removed }

  SetBkColor(0, 0, 0);
  for I := 0 to Count - 1 do
    Layer[I]^.Remove;

  RestoreBkColor;
end;

procedure THandler.TogglePage;
begin
  SetVisualPage(Curr);
  Curr := Last - Curr;
  SetActivePage(Curr);
  WaitForRetrace
end;

procedure THandler.PutStatic(AStatic : PStatic);
var
  I : integer;
begin
  if AStatic <> nil then
    for I := 0 to Last do
      with Stack[I] do
        if Num < MaxStatics - 1 then
        begin
          Inc(Num);
          Static[Num] := AStatic
        end
end;

procedure THandler.Idle;
var
  I : integer;
begin
  with Stack[Curr] do
    if Num <> -1 then
    begin
      Refresh := true;
      for I := 0 to Num do
        if Static[I]^.Valid then
          Static[I]^.Draw
        else
          Dispose(Static[I], Done);
      Num := -1
    end
end;

begin
  WriteLn(Name, ' v',version);
end.

