        .model TPascal
        .386

        .data

        EXTRN bytesperline:word
        EXTRN startbank   :word

        EXTRN setbank     :far

        EXTRN left, right :word
        EXTRN up, down    :word

        .code

;###########################################################################
;
; MACRO   : repmovesi - makes a rep movsb using movw and movb aligning to si
;
; PARAMS  :
;
; RETURNS :
;
; DESTROYS:
;

repmovesi MACRO
        local even, ok

        test  si, 1                             ; start word-aligned?
        jz even
        movsb                                   ; no - single byte
        dec   cx
even:   shr   cx, 1
        rep movsw                               ; now move words
        jnc ok                                  ; end word-aligned?
        movsb                                   ; no - single byte
ok:
        ENDM

;###########################################################################
;
; MACRO   : repmovedi - makes a rep movsb using movw and movb aligning to di
;
; PARAMS  :
;
; RETURNS :
;
; DESTROYS:
;

repmovedi MACRO
        local even, ok

        test  di, 1                             ; start word-aligned?
        jz even
        movsb                                   ; no - single byte
        dec   cx
even:   shr   cx, 1
        rep movsw                               ; now move words
        jnc ok                                  ; end word-aligned?
        movsb                                   ; no - single byte
ok:
        ENDM


;##########################################################################
;
; ROUTINE : putbitmap - puts a bitmap on the screen
;
; PARAMS  : px, py - screen coordinates, p - far pointer to TBitmap
;
; RETURNS :
;
; DESTROYS: ax bx cx dx si di es fs
;

        public putbitmap
putbitmap proc px:word, py:word, p:dword

        local bank      :word,                  \
              w         :word,                  \
              count     :word,                  \
              restofline:word,                  \
              xstart    :word,                  \
              ystart    :word,                  \
              xdiff     :word

        cld
        mov   ax, ds
        mov   fs, ax                            ; save data segment in fs

        xor   ax, ax
        mov   xstart, ax                        ; xstart:=0
        mov   ystart, ax                        ; ystart:=0
        mov   xdiff , ax                        ; xdiff :=0

        mov   ax, left                          ; CLIPPING
        cmp   ax, px                            ; left < px?
        jle noc1
        sub   ax, px
        mov   xstart, ax                        ; xstart:=left-px
        mov   xdiff , ax                        ; xdiff :=xstart
        mov   ax, left
        mov   px, ax                            ; px:=left
noc1:   mov   ax, up
        cmp   ax, py                            ; up < py?
        jle noc2
        sub   ax, py
        mov   ystart, ax                        ; ystart:=up-py
        mov   ax, up
        mov   py, ax                            ; py:=up
noc2:
        mov   ax, py
        mul   bytesperline
        add   ax, px
        adc   dx, startbank
        mov   bank, dx                          ; bank:=bank number
        mov   cx, ax                            ; save screenstart

        push  dx
        call setbank

        mov   ax, 0a000h
        mov   es, ax                            ; es:di -> screen
        lds   si, p                             ; ds:si -> bitmap

        lodsw                                   ; load width of bitmap
        mov   bx, ax                            ; save it in bx
        sub   ax, xstart
        mov    w, ax                            ; w:=width-xstart
        add   ax, px
        cmp   ax, [fs:right]                    ; x2 > right?
        jle noc3

        sub   ax, [fs:right]
        dec   ax
        sub    w, ax                            ; w:=w-(x2-right-1)
        add   xdiff, ax                         ; difference in one line
noc3:
        cmp   w , 0                             ; w<=0? -> exit
        jle exit1

        mov   ax, [fs:bytesperline]
        sub   ax, w
        mov   restofline, ax                    ; restofline = bytesperline-w

        lodsw                                   ; load height of bitmap
        sub   ax, ystart
        mov   count, ax                         ; count:=height-ystart

        add   ax, py
        cmp   ax, [fs:down]                     ; y2 > down?
        jle noc4

        sub   ax, [fs:down]
        dec   ax
        sub   count, ax                         ; count:=count-(y2-down-1)
noc4:
        cmp   count, 0                          ; height<=0? -> exit
        jle exit1

        mov   ax, ystart                        ; find start source line
        mul   bx
        add   si, ax
        add   si, xstart                        ; si:=si+p^.w*ystart+xstart

        mov   dx, cx

loop1:
        mov   di, dx                            ; di = line start adress
        mov   cx, w
        add   di, cx
        jc complication1                        ; there will be a bank switch
                                                ; while drawing this line!
        mov   di, dx

;        rep movsb
        repmovedi

weiter1:dec   count
        jz exit1

        add   si, xdiff
        add   dx, w
        add   dx, restofline
        jnc loop1

        inc   bank
        push  bank
        call setbank
        jmp loop1

complication1:
        sub   cx, di
        push  di

        mov   di, dx

;        rep movsb
        repmovedi

        inc   bank                               ; next bank
        push  bank
        call setbank

        pop   cx
        jcxz temp
        jmp cont4
temp:   jmp weiter1

cont4:
;        rep movsb
        repmovedi

        jmp weiter1

exit1:  mov   ax, fs
        mov   ds, ax                            ; restore data segment
        ret

endp    putbitmap


;##########################################################################
;
; ROUTINE : getbitmap - gets a bitmap from the screen
;
; PARAMS  : px, py - screen coordinates, w,h - size of bitmap,
;           p - far pointer to TBitmap
;
; RETURNS :
;
; DESTROYS: ax bx cx dx si di es fs
;

        public getbitmap
getbitmap proc px:word, py:word, w:word, h:word, p:dword

        local bank      :word,                  \
              count     :word,                  \
              restofline:word,                  \
              xstart    :word,                  \
              ystart    :word,                  \
              xdiff     :word,                  \
              oldw      :word

        cld
        mov   ax, ds
        mov   fs, ax                            ; save data segment in fs

        xor   ax, ax
        mov   xstart, ax                        ; xstart:=0
        mov   ystart, ax                        ; ystart:=0
        mov   xdiff , ax                        ; xdiff :=0

        mov   ax, left                          ; CLIPPING
        cmp   ax, px                            ; left < px?
        jle noc1b

        sub   ax, px
        mov   xstart, ax                        ; xstart:=left-px
        mov   xdiff , ax                        ; xdiff :=xstart
        mov   ax, left
        mov   px, ax                            ; px:=left

noc1b:  mov   ax, up
        cmp   ax, py                            ; up < py?
        jle noc2b

        sub   ax, py
        mov   ystart, ax                        ; ystart:=up-py
        mov   ax, up
        mov   py, ax                            ; py:=up

noc2b:  mov   ax, py
        mul   bytesperline
        add   ax, px
        adc   dx, startbank
        mov   bank, dx                          ; bank:=bank number
        mov   cx, ax                            ; save screenstart
        mov   si, ax

        push  dx
        call setbank

        mov   ax, 0a000h
        mov   ds, ax                            ; ds:si -> screen
        les   di, p                             ; es:di -> bitmap

        mov   ax, w
        mov   oldw, ax                          ; save width
        stosw                                   ; store width of bitmap
        mov   bx, ax                            ; save it in bx
        sub   ax, xstart
        mov    w, ax                            ; w:=width-xstart
        add   ax, px
        cmp   ax, [fs:right]                    ; x2 > right?
        jle noc3b

        sub   ax, [fs:right]
        dec   ax
        sub    w, ax                            ; w:=w-(x2-right-1)
        add   xdiff, ax                         ; difference in one line
noc3b:
        cmp   w , 0                             ; w<=0? -> exit
        jle exit1b

        mov   ax, [fs:bytesperline]
        sub   ax, w
        mov   restofline, ax                    ; restofline = bytesperline-w

        mov   ax, h
        stosw                                   ; store height of bitmap
        sub   ax, ystart
        mov   count, ax                         ; count:=height-ystart

        add   ax, py
        cmp   ax, [fs:down]                     ; y2 > down?
        jle noc4b

        sub   ax, [fs:down]
        dec   ax
        sub   count, ax                         ; count:=count-(y2-down-1)
noc4b:
        cmp   count, 0                          ; height<=0? -> exit
        jle exit1b

        mov   ax, ystart                        ; find start source line
        mul   oldw
        add   di, ax
        add   di, xstart                        ; di:=di+w*ystart+xstart

        mov   dx, cx

loop1b:
        mov   si, dx                            ; si = line start adress
        mov   cx, w
        add   si, cx
        jc complication1b                       ; there will be a bank switch
                                                ; while reading this line!
        mov   si, dx

;        rep movsb
        repmovesi

weiter1b:dec   count
        jz exit1b

        add   di, xdiff
        add   dx, w
        add   dx, restofline
        jnc loop1b

        inc   bank
        push  bank
        call setbank
        jmp loop1b

complication1b:
        sub   cx, si
        push  si

        mov   si, dx

;        rep movsb
        repmovesi

        inc   bank                               ; next bank
        push  bank
        call setbank

        pop   cx
        jcxz weiter1b

;        rep movsb
        repmovesi

        jmp weiter1b

exit1b: mov   ax, fs
        mov   ds, ax                            ; restore data segment
        ret

endp    getbitmap


;##########################################################################
;
; ROUTINE : putshape - put bitmap to screen without the "0" bytes
;
; PARAMS  : px, py - screen coordinates, p - far pointer to TBitmap
;
; RETURNS :
;
; DESTROYS: ax bx cx dx si di es fs
;

        public putshape
putshape proc px:word, py:word, p:dword

        local bank      :word,                  \
              w         :word,                  \
              count     :word,                  \
              restofline:word,                  \
              xstart    :word,                  \
              ystart    :word,                  \
              xdiff     :word

        cld
        mov   ax, ds
        mov   fs, ax                            ; save data segment in fs

        xor   ax, ax
        mov   xstart, ax                        ; xstart:=0
        mov   ystart, ax                        ; ystart:=0
        mov   xdiff , ax                        ; xdiff :=0

        mov   ax, left                          ; CLIPPING
        cmp   ax, px                            ; left < px?
        jle noc1a
        sub   ax, px
        mov   xstart, ax                        ; xstart:=left-px
        mov   xdiff , ax                        ; xdiff :=xstart
        mov   ax, left
        mov   px, ax                            ; px:=left
noc1a:  mov   ax, up
        cmp   ax, py                            ; up < py?
        jle noc2a
        sub   ax, py
        mov   ystart, ax                        ; ystart:=up-py
        mov   ax, up
        mov   py, ax                            ; py:=up
noc2a:
        mov   ax, py
        mul   bytesperline
        add   ax, px
        adc   dx, startbank
        mov   bank, dx                          ; bank:=bank number
        mov   cx, ax                            ; save screenstart

        push  dx
        call setbank

        mov   ax, 0a000h
        mov   es, ax                            ; es:di -> screen
        lds   si, p                             ; ds:si -> bitmap

        lodsw                                   ; load width of bitmap
        mov   bx, ax                            ; save it in bx
        sub   ax, xstart
        mov    w, ax                            ; w:=width-xstart
        add   ax, px
        cmp   ax, [fs:right]                    ; x2 > right?
        jle noc3a

        sub   ax, [fs:right]
        dec   ax
        sub    w, ax                            ; w:=w-(x2-right-1)
        add   xdiff, ax                         ; difference in one line
noc3a:
        cmp   w , 0                             ; w<=0? -> exit
        jle exit3

        mov   ax, [fs:bytesperline]
        sub   ax, w
        mov   restofline, ax                    ; restofline = bytesperline-w

        lodsw                                   ; load height of bitmap
        sub   ax, ystart
        mov   count, ax                         ; count:=height-ystart

        add   ax, py
        cmp   ax, [fs:down]                     ; y2 > down?
        jle noc4a

        sub   ax, [fs:down]
        dec   ax
        sub   count, ax                         ; count:=count-(y2-down-1)
noc4a:
        cmp   count, 0                          ; height<=0? -> exit
        jle exit3

        mov   ax, ystart                        ; find start source line
        mul   bx
        add   si, ax
        add   si, xstart                        ; si:=si+p^.w*ystart+xstart

        mov   dx, cx

loop3:
        mov   di, dx                            ; di = line start adress
        mov   cx, w
        add   di, cx
        jc complication3                        ; there will be a bank switch
                                                ; while drawing this line!
        mov   di, dx

sline1: mov   al, ds:[si]
        inc   si
        or    al, al
        jz nix1
        mov   es:[di], al
nix1:   inc   di
        dec   cx
        jnz sline1

weiter3:dec   count
        jz exit3

        add   si, xdiff
        add   dx, w
        add   dx, restofline
        jnc loop3

        inc   bank
        push  bank
        call setbank
        jmp loop3

complication3:
        sub   cx, di
        push  di

        mov   di, dx

sline3: mov   al, ds:[si]
        inc   si
        or    al, al
        jz nix3
        mov   es:[di], al
nix3:   inc   di
        dec   cx
        jnz sline3

        inc   bank                               ; next bank
        push  bank
        call setbank

        pop   cx
        jcxz weiter3                             ; wenn nichts mehr zu tun

sline2: mov   al, ds:[si]
        inc   si
        or    al, al
        jz nix2
        mov   es:[di], al
nix2:   inc   di
        dec   cx
        jnz sline2
        jmp weiter3

exit3:  mov   ax, fs
        mov   ds, ax                            ; restore data segment
        ret

endp    putshape


;##########################################################################
;
; ROUTINE : putshapeadd  - put bitmap to screen without the "0" bytes,
;                          and add a constant to each byte plotted
;
; PARAMS  : px, py - screen coordinates, color - number to add to each pixel,
;                p - far pointer to TBitmap
;
; RETURNS :
;
; DESTROYS: ax bx cx dx si di es fs
;

        public putshapeadd
putshapeadd  proc px:word, py:word, color:word, p:dword

        local bank      :word,                  \
              w         :word,                  \
              count     :word,                  \
              restofline:word,                  \
              xstart    :word,                  \
              ystart    :word,                  \
              xdiff     :word

        cld
        mov   ax, ds
        mov   fs, ax                            ; save data segment in fs

        xor   ax, ax
        mov   xstart, ax                        ; xstart:=0
        mov   ystart, ax                        ; ystart:=0
        mov   xdiff , ax                        ; xdiff :=0

        mov   ax, left                          ; CLIPPING
        cmp   ax, px                            ; left < px?
        jle noc1c
        sub   ax, px
        mov   xstart, ax                        ; xstart:=left-px
        mov   xdiff , ax                        ; xdiff :=xstart
        mov   ax, left
        mov   px, ax                            ; px:=left
noc1c:  mov   ax, up
        cmp   ax, py                            ; up < py?
        jle noc2c
        sub   ax, py
        mov   ystart, ax                        ; ystart:=up-py
        mov   ax, up
        mov   py, ax                            ; py:=up
noc2c:
        mov   ax, py
        mul   bytesperline
        add   ax, px
        adc   dx, startbank
        mov   bank, dx                          ; bank:=bank number
        mov   cx, ax                            ; save screenstart

        push  dx
        call setbank

        mov   ax, 0a000h
        mov   es, ax                            ; es:di -> screen
        lds   si, p                             ; ds:si -> bitmap

        lodsw                                   ; load width of bitmap
        mov   bx, ax                            ; save it in bx
        sub   ax, xstart
        mov    w, ax                            ; w:=width-xstart
        add   ax, px
        cmp   ax, fs:[right]                    ; x2 > right?
        jle noc3c

        sub   ax, fs:[right]
        dec   ax
        sub    w, ax                            ; w:=w-(x2-right-1)
        add   xdiff, ax                         ; difference in one line
noc3c:
        cmp   w , 0                             ; w<=0? -> exit
        jle exit3c

        mov   ax, fs:[bytesperline]
        sub   ax, w
        mov   restofline, ax                    ; restofline = bytesperline-w

        lodsw                                   ; load height of bitmap
        sub   ax, ystart
        mov   count, ax                         ; count:=height-ystart

        add   ax, py
        cmp   ax, fs:[down]                     ; y2 > down?
        jle noc4c

        sub   ax, fs:[down]
        dec   ax
        sub   count, ax                         ; count:=count-(y2-down-1)
noc4c:
        cmp   count, 0                          ; height<=0? -> exit
        jle exit3c

        mov   ax, ystart                        ; find start source line
        mul   bx
        add   si, ax
        add   si, xstart                        ; si:=si+p^.w*ystart+xstart

        mov   dx, cx
        mov   bx, color

loop3c:
        mov   di, dx                            ; di = line start adress
        mov   cx, w
        add   di, cx
        jc complication3c                       ; there will be a bank switch
                                                ; while drawing this line!
        mov   di, dx

sline1c:mov   al, ds:[si]
        inc   si
        or    al, al
        jz nix1c
        add   ax, bx
        mov   es:[di], al
nix1c:  inc   di
        dec   cx
        jnz sline1c

weiter3c:dec   count
        jz exit3c

        add   si, xdiff
        add   dx, w
        add   dx, restofline
        jnc loop3c

        inc   bank
        push  bank
        call setbank
        jmp loop3c

complication3c:
        sub   cx, di
        push  di

        mov   di, dx

sline3c:mov   al, ds:[si]
        inc   si
        or    al, al
        jz nix3c
        add   ax, bx
        mov   es:[di], al
nix3c:  inc   di
        dec   cx
        jnz sline3c

        inc   bank                               ; next bank
        push  bank
        call setbank

        pop   cx
        jcxz weiter3c                            ; wenn nichts mehr zu tun

sline2c:mov   al, ds:[si]
        inc   si
        or    al, al
        jz nix2c
        add   ax, bx
        mov   es:[di], al
nix2c:  inc   di
        dec   cx
        jnz sline2c
        jmp weiter3c

exit3c: mov   ax, fs
        mov   ds, ax                            ; restore data segment
        ret

endp    putshapeadd


;##########################################################################
;
; ROUTINE : putsprite - put bitmap to screen without the "0" bytes,
;                       add a constant to each byte plotted, and check
;                       given background with given mask to move 'under'
;                       certain bitplanes
;
; PARAMS  : px, py - screen coordinates
;            color - number to add to each pixel,
;                m - bit mask specifying which color groups are 'above'
;                    the bitmap (colors are grouped in 8 each)
;                p - far pointer to TBitmap
;               bg - far pointer to TBitmap which contains the background
;
; RETURNS :
;
; DESTROYS: ax bx cx dx si di es fs
;


        public putsprite
putsprite proc px:word, py:word, color:byte, m:dword, p:dword, bg:dword

        local bank      :word,                  \
              w         :word,                  \
              count     :word,                  \
              restofline:word,                  \
              xstart    :word,                  \
              ystart    :word,                  \
              xdiff     :word,                  \
              ofdiff    :word

        cld
        mov   ax, ds
        mov   gs, ax                            ; save data segment in gs

        xor   ax, ax
        mov   xstart, ax                        ; xstart:=0
        mov   ystart, ax                        ; ystart:=0
        mov   xdiff , ax                        ; xdiff :=0

;
; clipping part 1
;

        mov   ax, left                          ; CLIPPING
        cmp   ax, px                            ; left < px?
        jle noc1d

        sub   ax, px
        mov   xstart, ax                        ; xstart:=left-px
        mov   xdiff , ax                        ; xdiff :=xstart
        mov   ax, left
        mov   px, ax                            ; px:=left
noc1d:
        mov   ax, up
        cmp   ax, py                            ; up < py?
        jle noc2d

        sub   ax, py
        mov   ystart, ax                        ; ystart:=up-py
        mov   ax, up
        mov   py, ax                            ; py:=up


;
; init positions
;

noc2d:
        mov   ax, py
        mul   bytesperline
        add   ax, px
        adc   dx, startbank
        mov   bank, dx                          ; bank:=bank number
        mov   cx, ax                            ; save screenstart

        push  dx                                ; set startbank
        call setbank

        mov   ax, 0a000h
        mov   es, ax                            ; es:di -> screen
        lds   si, p                             ; ds:si -> bitmap
        lfs   bx, bg                            ; fs:bx -> background
        sub   bx, si
        mov   ofdiff, bx                        ; ofdiff:=si-bx

;
; clipping, 2nd part
;

        lodsw                                   ; load width of bitmap
        mov   bx, ax                            ; save it in bx
        sub   ax, xstart
        mov    w, ax                            ; w:=width-xstart

        add   ax, px
        cmp   ax, [gs:right]                    ; x2 > right?
        jle noc3d

        sub   ax, [gs:right]
        dec   ax
        sub    w, ax                            ; w:=w-(x2-right-1)
        add   xdiff, ax                         ; difference in one line
noc3d:
        cmp   w , 0                             ; w<=0? -> exit
        jle exitd
        mov   ax, [gs:bytesperline]
        sub   ax, w
        mov   restofline, ax                    ; restofline = bytesperline-w

        lodsw                                   ; load height of bitmap
        sub   ax, ystart
        mov   count, ax                         ; count:=height-ystart

        add   ax, py
        cmp   ax, [gs:down]                     ; y2 > down?
        jle noc4d

        sub   ax, [gs:down]
        dec   ax
        sub   count, ax                         ; count:=count-(y2-down-1)
noc4d:
        cmp   count, 0                          ; height<=0? -> exit
        jle exitd

;
; loop init
;

        mov   ax, ystart                        ; find start source line
        mul   bx
        add   si, ax
        add   si, xstart                        ; si:=si+p^.w*ystart+xstart

        mov   dx, cx
        mov   bx, ofdiff                        ; bx = offset difference

loop1d: push  dx                                ; save screen address
        mov   di, dx                            ; di = line start adress


;
; Processing a single line
;
;   si : start of line in source bitmap
;   ds : segment of source bitmap
;   fs : segment of background bitmap
;   bx : offset difference between background and source
;
; uses ax, bx, cx, dx, ds, fs, si, di

        mov   cx, w                             ; number of bytes to process
        add   di, cx
        jc complicationd                        ; there will be a bank switch
                                                ; while drawing this line!
        mov   di, dx
        mov   edx, m

slined: mov   ah, ds:[si]                       ; load sprite          1
        or    ah, ah                            ;                      1
        jz nixd                                 ; 'cookie cutting'     1/3

        mov   al, fs:[si+bx]                    ; load background      1
        shr   al, 3                             ; find color group     2
        bt    edx, eax                          ;                      3
        jc nixd                                 ; and mask it!         1/3

        add   ah, color                         ; modify color         2
        mov   es:[di], ah                       ; to screen memory     1
nixd:   inc   si                                ;                      1
        inc   di                                ;                      1
        dec   cx                                ;                      1
        jnz slined                              ;                      1/3

weiterd:
        pop   dx                                ; pointer to screen memory
        dec   count
        jz exitd

        add   si, xdiff                         ; move to next line
        add   dx, w
        add   dx, restofline
        jnc loop1d

        inc   bank                              ; bank switch between lines
        push  bank
        call setbank
        jmp loop1d

complicationd:
        sub   cx, di                            ; first half
        push  di

        mov   di, dx
        mov   edx, m                            ; load mask

slined2:mov   ah, ds:[si]                       ; load sprite          1
        or    ah, ah                            ;                      1
        jz nixd2                                ; 'cookie cutting'     1/3

        mov   al, fs:[si+bx]                    ; load background      1
        shr   al, 3                             ; find color group     2
        bt    edx, eax                          ;                      3
        jc nixd2                                ; and mask it!         1/3

        add   ah, color                         ; modify color         2
        mov   es:[di], ah                       ; to screen memory     1
nixd2:  inc   si                                ;                      1
        inc   di                                ;                      1
        dec   cx                                ;                      1
        jnz slined2                             ;                      1/3

        inc   bank                              ; next bank
        push  bank
        call setbank

        pop   cx                                ; second half
        jcxz weiterd
slined3:mov   ah, ds:[si]                       ; load sprite          1
        or    ah, ah                            ;                      1
        jz nixd3                                ; 'cookie cutting'     1/3

        mov   al, fs:[si+bx]                    ; load background      1
        shr   al, 3                             ; find color group     2
        bt    edx, eax                          ;                      3
        jc nixd3                                ; and mask it!         1/3

        add   ah, color                         ; modify color         2
        mov   es:[di], ah                       ; to screen memory     1
nixd3:  inc   si                                ;                      1
        inc   di                                ;                      1
        dec   cx                                ;                      1
        jnz slined3                             ;                      1/3

        jmp weiterd

exitd:  mov   ax, gs
        mov   ds, ax                            ; restore data segment
        ret

endp    putsprite


;##########################################################################
;
; ROUTINE : putpcx - put pcx coded bitmap to screen
;
; PARAMS  : px, py - screen coordinates, p - far pointer to TBitmap
;
; RETURNS :
;
; DESTROYS: ax bx cx dx si di es
;

        public putpcx
putpcx  proc buffer:dword,                              \
             bsize:word,                                \
             fillbuffer:dword,                          \
             x:word, y:word, w:word, h:word

        local line:word, restofline:word, bank:word, bend:word

        mov   ax, bytesperline                  ; calc rest of line
        sub   ax, w
        mov   restofline, ax

        mov   ax, 0a000h                        ; es -> screen memory
        mov   es, ax

        mov   ax, y                             ; calc start pos
        mul   bytesperline
        add   ax, x
        adc   dx, startbank
        mov   di, ax
        mov   bank, dx

        push  dx
        call setbank                            ; set startbank

        lfs   si, buffer
        add   si, bsize
        mov   bend, si                          ; bend = end of buffer
        lfs   si, buffer                        ; fs:si -> buffer
        mov   ax, h
        mov   line, ax                          ; line = h

outer_loop:
        xor   dx, dx                            ; dx   = 0

inner_loop:
        cmp   si, bend                          ; buffer empty?
        jl bufferok

        pusha                                   ; call fillbuffer
        push  es                                ; to reload data from disk
        call [fillbuffer]
        pop   es
        popa
        lfs   si, buffer

bufferok:
        mov   al, fs:[si]
        inc   si                                ; load byte
        cmp   al, 11000000b                     ; is compressed?
        jb not_compressed

        mov   ah, al
        and   al, 00111111b                     ; yes
        movzx cx, al                            ; cx = number of repeats
        add   dx, cx                            ; x  = x + cx

        cmp   si, bend
        jl bufferok2

        pusha
        push  es
        call [fillbuffer]
        pop   es
        popa
        lfs   si, buffer
bufferok2:
        mov   al, fs:[si]
        inc   si                                ; load byte to be repeated

reploop:
        mov   es:[di], al
        inc   di                                ; write byte
        jnz cont2                               ; bank switch?
        inc   bank
        push  bank
        call setbank
cont2:  dec   cx
        jnz reploop                             ; loop cx times
        jmp endfor

not_compressed:
        mov   es:[di], al
        inc   di                                ; write uncompressed byte
        jnz cont                                ; bank switch?
        inc   bank
        push  bank
        call setbank
cont:
        inc   dx                                ; next x

endfor:
        cmp   dx, w                             ; end of line?
        jl inner_loop

        dec   line                              ; one line down
        jz endfor2

        add   di, restofline                    ; move to start of next line
        jnc outer_loop                          ; bank switch?
        inc   bank
        push  bank
        call setbank
        jmp outer_loop

endfor2:
        ret

putpcx  endp

        end

