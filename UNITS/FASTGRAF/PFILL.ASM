
;      Standard-Polygonfuellroutine
;      ----------------------------
;
; hierzu gehoert  "POLYFILL.H"
; benoetigt "PFBASE.ASM"
;

        .model large
        .386

        .data



extrn  pfXOrigin:word       ; Lage des Clip-Fensters
extrn  pfYOrigin:word

extrn  Xres:word            ; size of clip window
extrn  Yres:word

extrn  maxx:word
extrn  maxy:word

; screen

extrn  ScrXres:word      ; Groesse der Bildschirmregion (physikal. oder virtuell),
extrn  ScrYres:word      ;  in die gezeichnet wird

extrn  pfScreenAdr:word         ; Segmentadresse des verwendeten Screens
extrn  pfLineAdr:dword          ; Zeilenanfangsadressen

extrn startbank:word     ; aus FASTGRAF

; Variablen fuer internen Gebrauch:

; Parameter werden hier gespeichert:
extrn  polygon:word
extrn  farbe:byte
extrn  anz:word

extrn  count :word
extrn  links :word
extrn  rechts:word
extrn  oben  :word
extrn  unten :word

extrn  lirrand:word
extrn  rerrand:word





@curseg ends

extrn polycontour:proc
extrn setbank:proc

        .code


msetbank MACRO banknum
            call setbank pascal,banknum
         ENDM


; ------ die Polygonf�llroutine --------

; void polyfill(polygon far *polygon, byte farbe, unsigned anz)
;
; - Polygon einfarbig fuellen
; - hoechstens "maxpolypunkte" Punkte
; - Polygon MUSS konvex sein

public polyfill
polyfill PROC
         ARG Panz:word,Pfarbe:word,Poly:dword=arglen

            push bp
            mov bp,sp

            mov ax,Pfarbe       ; Farbe speichert polycontour nicht
            mov [farbe],al

            call polycontour pascal,Poly,Panz

            or ax,ax            ; nichts zu Fuellen
            jz Fill_End

; Konturen Ausfuellen

            mov ax,[pfScreenAdr]                ; Screensegment
            mov es,ax

            mov bp,[unten]                      ; load line number
            sub bp,[oben]
            inc bp

            mov si,[oben]                       ; index in border tables
            shl si,1

            mov bx,[oben]                       ; address&bank of first line
            add bx,[pfYOrigin]
            shl bx,2
            mov dx,word ptr [pfLineAdr+bx]
            mov bx,word ptr [pfLineAdr+bx+2]
            add dx,[pfXOrigin]
            adc bx,[startbank]
            msetbank bx

            mov al,farbe                        ; load color
            mov ah,al
            cld                                 ; increment DI

; AX farbe
; BX banknr
; CX stosb counter
; DX lineadr
;
; DI
; SI index to lirrand and rerrand
; BP linecount

lineloop:   mov cx,dx
            add cx,[rerrand+si]
            jc trans1

            ; no transition left of rerrand
            mov di,[lirrand+si]
            add di,dx
            sub cx,di
            inc cx

            test di,1
            jz lefteven1
            stosb
            dec cx
lefteven1:  shr cx,1
            rep stosw
noword1:    jnc ok1
            stosb

ok1:        dec bp
            jz Fill_End
            add si,2
            add dx,ScrXres
            jnc lineloop

            inc bx
            msetbank bx
            jmp lineloop

            ; bank transition left of rerrand
trans1:     mov di,dx
            add di,[lirrand+si]
            jc lirtrans

            ; bank transition within scanline
            push cx
            mov cx,di
            neg cx

            test di,1                   ; first part
            jz lefteven2
            stosb
            dec cx
lefteven2:  shr cx,1
            rep stosw

            inc bx                      ; set new bank
            msetbank bx

            pop cx                      ; second part
            inc cx
            shr cx,1
            rep stosw
            jnc ok2
            stosb

ok2:        dec bp
            jz Fill_End
            add si,2
            add dx,ScrXres
            jmp lineloop


            ; bank transition left of lirrand
lirtrans:   inc bx                              ; set new bank
            msetbank bx

            sub cx,di
            inc cx

            test di,1
            jz lefteven3
            stosb
            dec cx
lefteven3:  shr cx,1
            rep stosw
noword3:    jnc ok3
            stosb

ok3:        dec bp
            jz Fill_End
            add si,2
            add dx,ScrXres
            jmp lineloop


Fill_End:   pop bp
            ret arglen
polyfill endp

            end










