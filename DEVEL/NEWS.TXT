===============================================================================
                               MULTIJOY NEWS
===============================================================================

Inzwischen gibt es so viele Multijoy-Enthusiasten, da� keiner mehr wei�, was
der andere tut. Deshalb habe ich (Henning) mich entschieden, einen kurzen
�berblick zu geben. In Zukunft k�nnt Ihr ja Eure eigenen Programme selbst
beschreiben (wenn Ihr wollt), denn wenn es neues gibt, k�nnte es auch neue
MULTIJOY NEWS geben.


-------------------------------------------------------------------------------
                            MULTIJOY 2.0 droht!
-------------------------------------------------------------------------------

Neue Hardware macht es m�glich, Joysticks mit bis zu drei Kn�pfen an das Multi
Joystick Interface anzuschlie�en. Jetzt k�nnen die Spiele endlich ein bi�chen
komplexer werden! Tank, Alien Invasion und Jetfight sind die ersten Kandidaten.
     Die alte Multijoy-Software ist leider nicht aufw�rtskompatibel, so da� die
Spiele alle neu kompiliert werden m�ssen, um sie mit der neuen Hardware
verwenden zu k�nnen. Die neue Software ist aber selbstverst�ndlich
abw�rtskompatibel, so da� die alten Interfaces immer noch funktionieren. (Sie
bieten nat�rlich nicht den Vorteil, 3-Knopf-Joysticks anschlie�en zu k�nnen.)


-------------------------------------------------------------------------------
                        Multijoy Hardware f�r alle!
-------------------------------------------------------------------------------

Heiko, Thomas und Henning haben einen Haufen Platinen des neuen Typs ge�tzt und
ein paar Interfaces fertiggebaut. Wer davor zur�ckscheut, selbst zu �tzen, kann
eine Platine zum Selbstkostenpreis kriegen, wer vorm L�ten kapituliert, kann
mal anfragen, ob er auch ein Fertigger�t bekommen kann.


-------------------------------------------------------------------------------
                         Konkurrenz f�r Multijoy?
-------------------------------------------------------------------------------

Auf die Ver�ffentlichung des ersten Multijoy-Pakets hin schrieb ein Hacker aus
Linz einen Brief an Christof, in dem er erz�hlte, er habe ein eigenes
Joystick-Interface f�r den Printer Port entwickelt, das sogar 8 Joysticks
unterst�tze, und einen Dynablaster-Clone daf�r geschrieben. Dieses Interface
hat den Vorteil, da� es ohne Chips auskommt, weil es eine Diodenmatrix
verwendet, und deshalb keine eigene Stromversorgung braucht. Die Versuche,
dieses Interface zu vermarkten, seien allerdings gescheitert.


-------------------------------------------------------------------------------
                            Multijoy f�r Linux!
-------------------------------------------------------------------------------

Thomas hat MINIJOY.C und JOYTEST.C f�r Linux umgesetzt. Au�erdem hat er eine
XChomp-Version an Multijoy angepa�t: Joystick 1 steuert den Pacman, und die
Joysticks 2 bis 5 steuern die Geister. (Armer Pacman!)


-------------------------------------------------------------------------------
                     Amiga-M�use am Multijoy Interface
-------------------------------------------------------------------------------

Da� man Atari-Trakballs an das Multi Joystick Interface anschlie�en kann, wurde
schon vor einiger Zeit ausprobiert. Jetzt sind auch Amiga-M�use nicht mehr
sicher: Mit dem Interface 2.0 kann man sechs von ihnen an den PC anschlie�en
und auch beide Kn�pfe abfragen. (Great - MultiMice!)
     Die M�use erfordern allerdings andere Software als die Trakballs: Atari
Trakballs geben ein Impulssignal f�r die Entfernung und ein Richtungsbit an den
Computer, w�hrend die M�use zwei um 90� phasenverschobene Impulssignale
abgeben, aus denen man die Richtung selbst bestimmen mu�.


-------------------------------------------------------------------------------
                         Neues Spiel: TANK BATTLE!
-------------------------------------------------------------------------------

Autor: Henning Katte. 6 Panzer, Team-Spiel, Vogelperspektive - die meisten von
Euch kennen Vorversionen von Tank Battle. Henning Katte will die
Zufallslandschaften noch durch designte Landschaften ersetzen, bevor er Tank
Battle als fertig ansieht. Au�erdem sollen verschiedene Panzer-Typen daf�r
sorgen, da� man die Teams so ausbalancieren kann, da� keine Seite (durch
K�nnen oder zahlenm��ige �berlegenheit) ein �bergewicht hat).
     Marcus Weber, Tank-Scharfsch�tze, zu dieser Aussicht: "Ich nehme den
Panzer mit der gr��ten Reichweite!" - kurze Pause - "Und den, der am feinsten
drehen kann!"


-------------------------------------------------------------------------------
                        Neues Spiel: ALIEN INVASION!
-------------------------------------------------------------------------------

Autor: Heiko. KEIN Space Invaders, sondern eher eine Art Space Hulk: Tapfere
Soldaten, die in einem Alien-verpesteten Raumschiff herumlaufen und mit vielen
verschiedenen Waffen alles �ber den Haufen schie�en, was sich bewegt. Das Spiel
ist dank vieler Extras sehr komplex, die Spielhandlung allerdings noch nicht.
Technisch ist das Spiel schon weit gediehen, aber die Spielbalance erfordert
noch etwas Arbeit.


------------------------------------------------------------------------------
                            Neues Spiel: BLOCKS!
------------------------------------------------------------------------------

Autoren: Henning und Felix. Endlich wieder ein Multijoy-Textmodus-Spiel!
Vorbild f�r Blocks war ein ZX-81-Spiel, und entsprechend primitiv ist es dann
auch: Die Spieler bewegen sich, abwechselnd ziehend, �ber den Bildschirm. Nach
jedem Zug erscheint auf einem der vier angrenzenden Felder ein Hindernis - ein
Block. Wer nicht mehr ziehen kann, weil die Blocks ihn blockieren, scheidet
aus. Henning zu Felix K�mmerlen, der Action-Spiele f�rchtet: "Ich glaube, ich
habe hier etwas f�r dich." Felix K�mmerlen, nachdem er zehnmal gegen Henning
gewonnen hat: "Ja! Das ist das beste von allen Multijoy-Spielen!"


------------------------------------------------------------------------------
                           Neues Spiel: JETFIGHT!
------------------------------------------------------------------------------

Autor: Henning. Jetfight 0.9 geh�rte schon zum MULTIJOY-1.0-Paket, aber
JETFIGHT 1.1 ist viel besser: Es gibt jetzt Wolken, zielsuchende Raketen, das
Looping-Man�ver und Digisounds.


------------------------------------------------------------------------------
                          Neues Spiel: ASTEROIDS!
------------------------------------------------------------------------------

Autoren: Christof und Henning. Asteroids ist ein schon etwas �lteres Projekt,
das sich aber der Fertigstellung n�hert. Die Spieler k�mpfen alle miteinander
gegen die Asteroiden, die Extraschiffe kommen von einem gemeinsamen Konto und
wenn ein Bildschirm abger�umt ist, bekommt jeder 'tote' Spieler ein neues
Leben. Die Entwicklung von Asteroids stagniert allerdings zur Zeit, weil
Henning Ruch Probleme mit Henning Kattes SPRITES-Unit hat.


------------------------------------------------------------------------------
                         Neue Anwendung: CHEMGRAF!
------------------------------------------------------------------------------

Autor: Peter Fiege. CHEMGRAF ist wahrscheinlich das beste und schnellste
Molek�l-Darstellungsprogramm, das man unter DOS kriegen kann. CHEMGRAF liest
MOL-Files, wie das g�ngigste Chemie-Programm sie erzeugt, und zeigt auf dem
Bildschirm ein Molek�l mit h�bschen bunten B�llen als Atomen und 3D-Linien als
Bindungen. CHEMGRAF verwendet eine Zentralprojektion und kann um alle drei
Achsen rotieren und zoomen. Zur Steuerung nimmt man nat�rlich einen Joystick,
notfalls auch die Tastatur. CHEMGRAF ist das erste ernsthafte
Multijoy-Programm!


------------------------------------------------------------------------------
                         Neues Projekt: BROADSIDES!
------------------------------------------------------------------------------

Autor: Christof. Broadsides ist ein Taktik-Spiel mit Simulationselementen, das
sich an einem C-64-Vorbild orientiert. Christof hat Broadsides gerade erst
angefangen, aber das Grundger�st steht schon.


------------------------------------------------------------------------------
                         Neues Projekt: HANDSCHUH!
------------------------------------------------------------------------------

Autor: Peter. Handschuh ist wahrscheinlich an Gauntlet orientiert und das erste
Multijoy-Spiel, das sechs Fenster unabh�ngig scrollt. Bisher kann man nur mit
sechs kleinen M�nnchen durch ein Zufallslabyrinth rennen, aber Peter hat auch
gerade erst angefangen.


------------------------------------------------------------------------------
                        Neues Projekt: 3D SPACE TAXI!
------------------------------------------------------------------------------

Autor: Felix. 3D Space Taxi ist ein anspruchsvolles 3D-Liniengrafik-Spiel. 3D
Taxi fordert ziemlich viel Rechenleistung vom Computer und ziemlich viel
Geschick vom Spieler. Jeder Spieler hat sein eigenes 3D-Fenster, und mit 6
Spielern werden die Fenster ziemlich klein und das Spiel etwas ruckelig. Der
Spielgedanke ist, Passagiere von Plattformen in aufzusammeln und sie auf
anderen Plattformen abzuliefern. Je schneller man ankommt und je sanfter man
landet, desto mehr Punkte bekommt man daf�r. Die Steuerung ist �hnlich wie bei
Zarch/Virus: Das Taxi reitet auf einem Schubstrahl, der senkrecht aus dem Taxi
austritt. Kippt man das Taxi nach vorne, kann man beschleunigen, kippt man das
Taxi zur Seite, fliegt man seitw�rts und dreht sich. 3D Space Taxi ist zwar
faszinierend, aber bisher noch kein Spiel.


------------------------------------------------------------------------------
                       RotRacer wird weiterentwickelt!
------------------------------------------------------------------------------

Ein Multijoy-Spiel, da� vor etwa einem Jahr Begeisterung hervorrief, ist
RotRacer. Walter Schmies hat mit der 2D-Vektorgraphik-Landschaft, die unter dem
Auto rotiert und scrollt, wohl jeden beeindruckt.
     Wie viele Spiele kam RotRacer in eine Phase, in der die Technik
funktioniert, aber die Spielbalance nicht stimmt, und dort blieb RotRacer
anscheinend eine Weile stecken. Christof erz�hlt, Walter habe RotRacer jetzt
wieder aufgegriffen und sei dabei, die Landschaft sogar mit Texturen zu
versehen.


------------------------------------------------------------------------------
                           Neues Tool: SPRITES!
------------------------------------------------------------------------------

Autor: Henning Katte. Urspr�nglich f�r Tank geschrieben, ist die neue
SPRITES-Unit von Henning Katte jetzt allgemein verwendbar. (Asteroids und
Broadsides bauen darauf auf.) SPRITES ist objektorientiert und �bernimmt Double
Buffering und Kollisionsabfrage, so da� man die l�stigen und immer
wiederkehrenden Probleme, die Grafik 'von Hand' aufzubauen, ein f�r allemal los
ist.
     Christof zu diesem Thema: "Mit der neuen Sprite-Unit schreibt man ein
Spiel an einem Abend!" Henning: (Komm-auf-den-Teppich-Blick). Christof: "Na
gut, ich wei� auch, da� das nicht stimmt, aber das Grundger�st steht an einem
Abend!" Sp�ter: Henning zu Henning Katte: "Ich fange kein neues Spiel mehr an,
solange ich die Sprite-Unit nicht habe!"


------------------------------------------------------------------------------
                           Neues Tool: VOCPLAY!
------------------------------------------------------------------------------

Autoren: Christian und Henning. VOCPLAY wurde urspr�nglich von Christian f�r
sein Ein-Spieler-Asteroids geschrieben. Henning hat das alte VOCPLAY jetzt
umgekrempelt, modernisiert und f�r Jetfight verwendet. VOCPLAY verwendet den
CT-VOICE-Treiber, der mit jeder Sound-Blaster mitgeliefert wird, und spielt
immer nur ein Sample zur Zeit �ber DMA ab. Diese Einschr�nkung ist zwar etwas
unangenehm, aber wenn man sich auf kurze Samples beschr�nkt und nicht zu viele
Ereignisse im Spiel ein Sample erfordern, kann man sehr gut damit auskommen.


------------------------------------------------------------------------------
                          Neues Tool: HZDETECT!
------------------------------------------------------------------------------

Autor: Henning. HZDETECT ist zwar schon ein bi�chen �lter, aber wahrscheinlich
hat bisher niemand dieses Tool bemerkt. HZDETECT mi�t die augenblickliche
Bildschirmfrequenz, so da� man die Ablaufgeschwindigkeit der Programme daran
anpassen kann. Wer einmal versucht hat, Jetfight 0.9 auf einem 17"-Monitor mit
100 Hz zu spielen, wei�, wozu HZDETECT gut ist.


------------------------------------------------------------------------------
                          Neues Tool: SHOWHELP!
------------------------------------------------------------------------------

Autor: Henning. SHOWHELP ist dazu gedacht, die Anleitung zu einem Spiel auf dem
Grafikschirm anzuzeigen. Man kann im Hilfstext mit Page Up/Down bl�ttern oder
das Inhaltsverzeichnis als Men� benutzen, um gezielt irgendwo hinzuspringen.
Ziemlich schlicht, aber besser, als die Anleitung nur als ASCII-File
beizulegen.


------------------------------------------------------------------------------
                              Wer macht was?
------------------------------------------------------------------------------

Christian     = Christian Wiese
                - Ein-Spieler-Asteroids
                - PROTIMER
                - Ur-VOCPLAY
Christof      = Christof Ruch
                - FASTGRAF
                - SCORESCR
                - DYNAMITE MEN
                - BROADSIDES
Felix         = Felix Meyer
                - 3D SPACE TAXI
Heiko         = Heiko Z�gel
                - ALIEN INVASION
Henning       = Henning Ruch
                - Doku
                - Layout
                - MULTIJOY
                - VOCPLAY
                - HZDETECT
                - SHOWHELP
                - JETFIGHT
                - TJOST
                - BLOCKS
                - ASTEROIDS
Henning Katte = Henning Katte
                - TANK BATTLE
                - SPRITES
Peter         = Peter Waldheim
                - TRON
                - HANDSCHUH
Peter Fiege   = Peter Fiege
                - MULTRIS
                - CHEMGRAF
Thomas        = Thomas Mielke
                - MINIJOY.C
                - Linux-Support
Walter        = Walter Schmies
                - ROTRACER


------------------------------------------------------------------------------
                                 'Impressum'
------------------------------------------------------------------------------

Diese ersten Multijoy News wurden in den Computer getippt von

      Henning Ruch
      04101/691681
      Richard-K�hn-Str. 39b
25421 Pinneberg

Sagt mir Bescheid, wenn Ihr etwas Neues macht, und wenn Ihr wollt, beschreibt
es selber. Die Multijoy News sollen alle kreativen Leute auf dem Laufenden
halten, denn inzwischen passiert so viel, da� die Mund-zu-Mund-Daten�bertragung
nicht mehr so furchtbar toll funktioniert.
