# instrument bank file

name test1

carrier

level    0
amvib    0 0
keyboard 0 0
adsr_eg  15 15 0 15 1
wave_mult 3 0

modulator

level    0
amvib    0 0
keyboard 0 0
adsr_eg  15 15 0 15 1
wave_mult 3 0

feedbackfm 1 0

next


name test2

carrier

level    0
amvib    0 0
keyboard 0 0
adsr_eg  15 15 0 15 1
wave_mult 3 0

modulator

level    0
amvib    0 0
keyboard 0 0
adsr_eg  15 15 0 15 1
wave_mult 3 0

feedbackfm 1 0

save_ibk test1
