Program voc2vox;

(*************************************************************************)
(*                                                                       *)
(* VOC2VOX V0.90                                                         *)
(*                                                                       *)
(* written in 1995 by Henning Ruch                                       *)
(* based on PCX2BTM V1.03 by Christof Ruch                               *)
(*                                                                       *)
(*************************************************************************)
(*                                                                       *)
(* Version 0.50                                                          *)
(*   - first functional version                                          *)
(*                                                                       *)
(*************************************************************************)

uses vocplay,
     vocsound,
     dos,
     crt;

var out         : FILE;

var source      : text;
    line        : string;
    outputname  : string;

    srcpos      : integer;
    srclen      : integer;

    nextchar    : char;

    srcline     : string;


function upstring (s : string) : string;
(* returns string in which all lower case character of S are converted *)
(* into upper case characters *)
var i : integer;
begin
  for i := 1 to length (s) do s [i] := upcase (s [i]);
  upstring := s;
end;


function nextword : string;
(* parses string LINE and returns next word specified by spaces *)
var p : integer;
begin
  if line <> '' then begin
    srcpos := srcpos + srclen;
    while line [1] = ' ' do begin
      line := copy (line, 2, length (line) - 1);
      inc (srcpos);
    end;
    p := pos (' ', line);
    if p = 0 then p := length (line) + 1;
    nextword := copy (line, 1, p - 1);
    srclen := p;
    line := copy (line, p + 1, length (line) - p);
  end else
    nextword := '';
end;


procedure select_dest (dest_name : string);
(* select new destination file, close old file if necessary *)
begin
  if outputname<>'' then closevox (out);

  outputname := dest_name;
  if not rewritevox (out, dest_name) then begin
    writeln ('VOC2VOX error:');
    writeln ('unable to write ', upstring (dest_name));
    halt;
  end;
end;


procedure select_sorc (sorc_name : string);
var s : file;
    v : pvoc;
(* select new VOC source file and write content to destination file *)
begin
  if outputname = '' then begin
    writeln ('VOC2VOX error:');
    writeln ('unable to write ', upstring (sorc_name), ' because no destination specified');
    halt;
  end;
  if not loadvoc (v, sorc_name) then begin
    writeln ('VOC2VOX error:');
    writeln ('unable to read ', upstring (sorc_name));
    halt;
  end;
  if not savevoc2vox (out, v) then begin
    writeln ('VOC2VOX error:');
    writeln ('unable to write ', upstring (sorc_name), ' to ', upstring (outputname));
    halt;
  end;
end;


procedure tick;
(* ticks once *)
begin
  sound (440);
  delay (10);
  nosound;
end;


procedure show_error;
(* shows error message *)
var i : integer;
begin
  writeln ('VOC2VOX error:');
  writeln ('unknown keyword encountered');
  writeln;
  writeln (srcline);
  for i := 1 to srcpos do write (' ');
  for i := srcpos + 1 to srcpos + srclen - 1 do write ('^');
  writeln;
  halt;
end;


function case_no (keyword : string) : integer;
(* generates an ordinal value for requested keyword for CASE evaluation *)
const keyword_no = 4;
      key        : array [1 .. keyword_no] of string
                 = ('#', 'destination', 'source', 'tick');
var   found      : boolean;
      i          : integer;
begin
  found := false;
  i := 0;
  repeat
    inc (i);
    if keyword = key [i] then found := true;
  until (i = keyword_no) or found;
  if found then case_no := i
           else show_error;
end;


var  wort      : string;
     pausewait : boolean;
     i         : integer;
(* VOC2VOX *)
begin
  outputname  :='';
  nextchar    :=' ';

  writeln('VOC2VOX V0.90 - written in 1995 by Henning Ruch');
  writeln;

  if paramcount < 1 then begin
    writeln ('Usage: VOC2VOX <filename>');
    writeln;
    writeln ('       <filename> specifies conversion control file');
    writeln ;
    writeln ('Copies VOC sample files into single VOX file');
    writeln ('For details see VOC2VOX.DOC');
  end else begin
    {$I-}
    assign (source, paramstr (1));
    reset (source);
    if ioresult <> 0 then begin
      assign (source, paramstr (1) + '.TXT');
      reset (source);
    end;
    {$I+}
    if ioresult = 0 then begin

      while not eof(source) do begin
        if line = '' then begin
          readln (source, srcline);
          srcpos := 0;
          srclen := 0;
          line := srcline;
        end;
        wort := nextword;
        if wort <> '' then
          case case_no (wort) of
             1 : line := '';                        (* #           *)
             2 : select_dest (nextword);            (* destination *)
             3 : select_sorc (nextword);            (* source      *)
             4 : tick;                              (* tick        *)
          end;
      end;

      if outputname <> '' then closevox (out);
      close (source);
    end else begin
      (* file not found *)
      writeln ('file ', paramstr(1), ' not found');
    end;
  end;
end.