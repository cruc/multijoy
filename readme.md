Install Turbo Pascal from here: http://techapple.net/2014/04/turbo-pascal-7-windows-7-windows-8-8-132bit-64bit-fullscreen-single-installer/

Some of our projects need TASM as well, I found one here: http://trimtab.ca/2010/tech/tasm-5-intel-8086-turbo-assembler-download/

Packaging was done way back then with ARJ. There is a free download for a DOS arj here: http://arj.sourceforge.net/
The download is an installer to be run in DOS... I ran it and just let it extract the arj into the c:\tp\bin directory so I knew it was in the path.

Edit the configuration in C:\TP7\Techapple.net\tpmodeauto.conf:

1. Find the section with the mount command and add a mount to make the repository root (where this readme is) the D:\ drive, e.g.
2. Make sure the TP binaries are in the path by doing this before the call to turbo in the same file, as well as our own Tools directory
3. Add a set environment command to the multijoy root, which is now d:\, also before the call to Turbo
4. In order to run the games in the DOS shell of Pascal with Keyboard emulation enabled, also set the keyboard configuration file 
5. And for the graphics of the FASTGRAF-based games to work we currently require a magic environment variable overriding the autodetection
of the VGA card.
  
Add the following block after the other mount commands:

```
  mount D: "g:\frueher\multijoy\2017"
  SET PATH=z:\;c:\tp\bin;d:\tools
  SET MULTIPATH=d:\
  SET MULTICFG=keyboard.cfg
  SET VESA=X
```  
  
After starting Turbo Pascal, you need to set the TPU paths in the appropriate menu, and either select your primary file (e.g. dynamite.pas) to 
compile and run it, or use the make command to build by going into a shell with File... Dos Shell... and running make in the appropriate 
directory.
