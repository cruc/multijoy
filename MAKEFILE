#
# this is the global MULTIJOY makefile
#
# it makes everything everywhere
#
# this makefile must be started in MULTIPATH
#

MAKE = make -S

all: units games tools

units:
        cd $(MULTIPATH)\UNITS
        $(MAKE)
        cd $(MULTIPATH)

arj: games
        cd $(MULTIPATH)\OFFICIAL
        $(MAKE) arj
        cd $(MULTIPATH)

games:
        cd $(MULTIPATH)\OFFICIAL
        $(MAKE)
        cd $(MULTIPATH)

tools:
        cd $(MULTIPATH)\devel
        $(MAKE) tools
        cd $(MULTIPATH)

clean:
        cd $(MULTIPATH)\UNITS
        $(MAKE) clean
        cd $(MULTIPATH)\OFFICIAL
        $(MAKE) clean
        cd $(MULTIPATH)\DEVEL
        $(MAKE) clean
        cd $(MULTIPATH)

realclean:
        cd $(MULTIPATH)\UNITS
        $(MAKE) realclean
        cd $(MULTIPATH)\OFFICIAL
        $(MAKE) realclean
        cd $(MULTIPATH)\DEVEL
        $(MAKE) realclean
        cd $(MULTIPATH)

